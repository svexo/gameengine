﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Base;
using Arena.Players;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Arena
{
    public class UserInterface
    {
        static Vector2 HealthBarLocation = new Vector2(512,690);
        static Vector2 AbilityStartingsLocation = new Vector2(350, 715);
        const int AbilityOffsetX = 100;

        static Team[] Rounds = new Team[3]{Team.None,Team.None,Team.None};
        static int Index = 0;

        private Player player;
        public UserInterface(Player player)
        {
            this.player = player;
        }
        public void SetRound(Team team)
        {
            Rounds[Index] = team;
            Index++;
        }
        public void ResetRounds() {
            Index = 0;
            Rounds = new Team[3] { Team.None, Team.None, Team.None };
        }
        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, Microsoft.Xna.Framework.GameTime gameTime)
        {
            //Drawing Background
            spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["UI_Back"], new Vector2(0,638), Color.White);
            //Drawing HealthBar
            //http://i.imgur.com/3IkHR.png
            Texture2D BackGround = (Texture2D)ArenaGame.GameRecources["HpBackground"];
            Texture2D GreenPart = (Texture2D)ArenaGame.GameRecources["HpForeground"];
            SpriteFont font = (SpriteFont)ArenaGame.GameRecources["HpFont"];
            spriteBatch.Draw(BackGround, HealthBarLocation, new Rectangle(0, 0, BackGround.Width, BackGround.Height), Color.White, 0f, new Vector2(BackGround.Width / 2, BackGround.Height / 2), 1, SpriteEffects.None, 0.21f);
            spriteBatch.Draw(GreenPart, HealthBarLocation, new Rectangle(0, 0, GreenPart.Width - (100 - (int)player.HealthPoints) * 3, GreenPart.Height), Color.White, 0f, new Vector2(GreenPart.Width / 2, GreenPart.Height / 2), 1, SpriteEffects.None, 0.22f);
            spriteBatch.DrawString(font, player.HealthPoints.ToString(), new Vector2(HealthBarLocation.X, HealthBarLocation.Y + 18), Color.WhiteSmoke, 0f, new Vector2(font.MeasureString(player.HealthPoints.ToString()).X / 2, font.MeasureString(player.HealthPoints.ToString()).Y + 2), 1, SpriteEffects.None, 0.23f);
            //- (font.MeasureString(player.HealthPoints.ToString()).X / 2))

            Vector2 AbilityLocation = AbilityStartingsLocation;
            Vector2 RoundLocation;
            //Drawing Ab1
            //spriteBatch.Draw(Texture, Position, null, Color.White, Rotation, Origin, 1f, SpriteEffects.None, layerDepth);
            spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["30x30" + player.AbilityA_TextureName], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.24f);
            if (player.AbilityA_Cooldown) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["RedBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            else spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["GreenBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            AbilityLocation.X += AbilityOffsetX;
            //Drawing Ab2
            spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["30x30" + player.AbilityB_TextureName], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.24f);
            if (player.AbilityB_Cooldown) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["RedBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            else spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["GreenBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            AbilityLocation.X += AbilityOffsetX;
            //Drawing Ab3
            spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["30x30" + player.AbilityC_TextureName], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.24f);
            if (player.AbilityC_Cooldown) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["RedBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            else spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["GreenBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            AbilityLocation.X += AbilityOffsetX;
            //Drawing Ab4
            spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["30x30" + player.AbilityD_TextureName], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.24f);
            if (player.AbilityD_Cooldown) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["RedBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
            else spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["GreenBorder"], AbilityLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);

            RoundLocation = new Vector2(AbilityLocation.X + 300, 658);
            for (int i = 0; i < 3; i++)
            {
                if (Rounds[i] == Team.None) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["round_gray"], RoundLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
                else if (Rounds[i] == Team.Blue) spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["round_blue"], RoundLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
                else spriteBatch.Draw((Texture2D)ArenaGame.GameRecources["round_red"], RoundLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.25f);
                RoundLocation.Y += 32;

            }
        }
    }
}
