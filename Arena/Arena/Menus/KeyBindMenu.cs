﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Base;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Arena.Menus
{
    public class KeyBindMenu : Menu
    {
        private Button bindbutton;
        private object control;
        public override void Update()
        {
            base.Update();
            var state = Keyboard.GetState();
            if (state.GetPressedKeys().Count() > 0)
            {
                if (state.GetPressedKeys().Last() != Keys.None)
                {
                    GameOptions.Controls[control] = state.GetPressedKeys().Last();
                    bindbutton.Text = control.ToString() + ": " + GameOptions.Controls[control].ToString();
                    ButtonEvents.MenuHandle.ShowMenu("options");
                }
            }
        }
        public void SetKeyBindMenu(Button bindbutton,object control)
        {
            this.bindbutton = bindbutton;
            this.control = control;
            ButtonEvents.MenuHandle.ShowMenu("keybindmenu");
        }

    }
}
