﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Base;
using Microsoft.Xna.Framework;
using Arena.Players.Characters;
using Arena.Networking;
using GameEngine.Networking;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Arena
{
    public static class ButtonEvents
    {
        public static MenuHandler MenuHandle;


        public static void onMouseEnter(Button sender, EventArgs args)
        {
            sender.TextColor = Color.Blue;
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["button_hover"]);
        }
        public static void onMouseLeave(Button sender, EventArgs args)
        {
            sender.TextColor = Color.Black;
        }
        public static void PlayButtononClick(Button sender, EventArgs args)
        {
            ArenaGame.NetPlayers.Clear();
            MenuHandle.ShowMenu("playmenu");
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["playbutton"]);
        }
        public static void OptionButtonsonClick(Button sender, EventArgs args)
        {
            MenuHandle.ShowMenu("options");
        }
        public static void ExitButtononClick(Button sender, EventArgs args)
        {
            System.Windows.Forms.Application.Exit();
        }
        public static void _1vs1ButtonClick(Button sender,EventArgs e)
        {
            var message = new NetMessage() { Data = new object[] {(byte)100} };
            ArenaGame.MasterClient.Send<NetMessage>(MessageType.MACTHMAKING, message);
            MenuHandle.ShowMenu("waitmenu");
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["button_click"]);
        }
        public static void _2vs2ButtonClick(Button sender, EventArgs e)
        {
            var message = new NetMessage() { Data = new object[] { (byte)101 } };
            ArenaGame.MasterClient.Send<NetMessage>(MessageType.MACTHMAKING, message);
            MenuHandle.ShowMenu("waitmenu");
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["button_click"]);
        }
        public static void _3vs3ButtonClick(Button sender, EventArgs e)
        {
            var message = new NetMessage() { Data = new object[] { (byte)102 } };
            ArenaGame.MasterClient.Send<NetMessage>(MessageType.MACTHMAKING, message);
            MenuHandle.ShowMenu("waitmenu");
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["button_click"]);
        }
        public static void WaitButtonClick(Button sender, EventArgs e)
        {
            MenuHandle.ShowMenu("characterselect");
        }
        public static void HealerButtonClick(Button sender, EventArgs e)
        {
            ArenaGame.Me = new Healer() { Username = ArenaGame.Username };
            ArenaGame.GameClient.Send<NetMessage>(MessageType.CHARACTERSELECT, new NetMessage() { Data = new object[] { ArenaGame.Username, 100} });
            sender.TextColor = Color.Red;
            MenuHandle.CurrentMenu.DisableEvents();
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["choisebutton"]);
            ArenaGame.GUI = new UserInterface(ArenaGame.Me);
        }
        public static void MageButtonClick(Button sender, EventArgs e)
        {
            ArenaGame.Me = new Mage() { Username = ArenaGame.Username };
            ArenaGame.GameClient.Send<NetMessage>(MessageType.CHARACTERSELECT, new NetMessage() { Data = new object[] { ArenaGame.Username, 101 } });
            sender.TextColor = Color.Red;
            MenuHandle.CurrentMenu.DisableEvents();
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["choisebutton"]);
            ArenaGame.GUI = new UserInterface(ArenaGame.Me);

        }
        public static void WarriorButtonClick(Button sender, EventArgs e)
        {
            ArenaGame.Me = new Warrior() { Username = ArenaGame.Username };
            ArenaGame.GameClient.Send<NetMessage>(MessageType.CHARACTERSELECT, new NetMessage() { Data = new object[] { ArenaGame.Username, 102 } });
            sender.TextColor = Color.Red;
            MenuHandle.CurrentMenu.DisableEvents();
            SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["choisebutton"]);
        }
        public static void ApplyButtonClick(Button sender, EventArgs e)
        {
            MenuHandle.ShowMenu("main");
        }

    }
}
