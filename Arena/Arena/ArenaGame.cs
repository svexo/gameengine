using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using GameEngine.Base;
using GameEngine.Physics;
using GameEngine.Networking;
using Arena.Players;
using Arena.Players.Characters;
using System.IO;
using Arena.Networking;
using Arena.Players.Ability;
using System.Diagnostics;
using GameEngine.Physics.Collision;
using Arena.Menus;


namespace Arena
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class ArenaGame : Microsoft.Xna.Framework.Game
    {
        public static string IP = "94.226.31.76";
        public static Client MasterClient; 
        public static Client GameClient;
        public static Dictionary<string, object> GameRecources = new Dictionary<string, object>();
        public static Player Me;
        public static UserInterface GUI;
        public static string Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        public static List<Player> NetPlayers = new List<Player>();
        public static List<GameObject> NetObjects = new List<GameObject>();
        public static System.Windows.Forms.Control InvokeControl = new System.Windows.Forms.Control();

        public static Vector2 RedSpawn = new Vector2(500, 200);
        public static Vector2 BlueSpawn = new Vector2(200, 200);

        public static string LogFile = "Logfile.log";
        //��//
        //LOCAL ABILITIES --> Check collsions towards NetPlayers (Netobjects is irrelevant since the local client will keep track of the abilities) ! MOVED TO PROJECTILE.CS 
        //��//

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameObject Wall;
 
       // Texture2D FireBall;
        public SpriteFont Font;
        MenuHandler MenuHandle;

        public static string Username;

        public ArenaGame(string Username)
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            GameOptions.SetDefaults(1024,768, ref graphics);
            ArenaGame.Username = Username;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.Window.Title = "Arena";
            GameOptions.Controls.Add("left", Keys.Q);
            GameOptions.Controls.Add("right", Keys.D);
            GameOptions.Controls.Add("up", Keys.Z);
            GameOptions.Controls.Add("down", Keys.S);
            GameOptions.Controls.Add("abilitya", Keys.D1);
            GameOptions.Controls.Add("abilityb", Keys.D2);
            GameOptions.Controls.Add("abilityc", Keys.D3);
            GameOptions.Controls.Add("abilityd", Keys.D4);
            //graphics.ToggleFullScreen();
            this.IsMouseVisible = true;
            MenuHandle = new MenuHandler();
            MasterClient.DataRecieved += new EventHandler<OnRecieve>(MasterDataRecieved);
            ButtonEvents.MenuHandle = MenuHandle;
            base.Initialize();
            #region Version Number
            EngineAction action = new EngineAction();
            action.Time = EngineAction.InfiniteTime;
            action.onDraw = (SpriteBatch sprite, GameTime time) =>
            {
                SpriteFont font = (SpriteFont)ArenaGame.GameRecources["HpFont"];
                
                sprite.DrawString(font,Version, new Vector2(graphics.PreferredBackBufferWidth - font.MeasureString(Version).X, 0), Color.Black);

            };
            ActionHandler.AddAction(action);
            #endregion
            ActionHandler.Start();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Wall = new GameObject(true);
            #region Loading Assets
            List<string> Textures = new List<string>(), Fonts = new List<string>(), SoundEffects = new List<string>(), Songs = new List<string>();
            GetAllFiles(Path.Combine(Content.RootDirectory,"Textures"), ref Textures);
            GetAllFiles(Path.Combine(Content.RootDirectory,"Fonts"),ref Fonts);
            GetAllFiles(Path.Combine(Content.RootDirectory,"SoundEffects"),ref SoundEffects);
            GetAllFiles(Path.Combine(Content.RootDirectory, "Songs"), ref Songs);

            foreach (string File in Textures)
            {
                string AssetLocation = File.Substring(File.IndexOf('\\') + 1, File.Length - 4 - (File.IndexOf('\\') + 1));
                string AssetName = File.Substring(File.LastIndexOf('\\') + 1, File.Length - 4 - (File.LastIndexOf('\\') + 1));
                GameRecources.Add(AssetName, Content.Load<Texture2D>(AssetLocation));
               ((Texture2D)GameRecources[AssetName]).Name = AssetName;               
            }
            foreach (string File in Fonts)
            {
                string AssetLocation = File.Substring(File.IndexOf('\\') + 1, File.Length - 4 - (File.IndexOf('\\') + 1));
                string AssetName = File.Substring(File.LastIndexOf('\\') + 1, File.Length - 4 - (File.LastIndexOf('\\') + 1));
                GameRecources.Add(AssetName, Content.Load<SpriteFont>(AssetLocation));
                //((SpriteFont)GameRecources[AssetName]).Name = AssetName;      
            }
            foreach (string File in SoundEffects)
            {
                    string AssetLocation = File.Substring(File.IndexOf('\\') + 1, File.Length - 4 - (File.IndexOf('\\') + 1));
                    string AssetName = File.Substring(File.LastIndexOf('\\') + 1, File.Length - 4 - (File.LastIndexOf('\\') + 1));
                    GameRecources.Add(AssetName, Content.Load<SoundEffect>(AssetLocation));
            }
            foreach (string File in Songs)
            {
                string AssetLocation = File.Substring(File.IndexOf('\\') + 1, File.Length - 4 - (File.IndexOf('\\') + 1));
                string AssetName = File.Substring(File.LastIndexOf('\\') + 1, File.Length - 4 - (File.LastIndexOf('\\') + 1));
                GameRecources.Add(AssetName, Content.Load<Song>(AssetLocation));
            }
            #endregion
            Wall.Texture = (Texture2D)GameRecources["UI_Back"];
            Wall.Position = new Vector2(Wall.Texture.Width / 2, graphics.PreferredBackBufferHeight - Wall.Texture.Height / 2);
            Font =(SpriteFont)GameRecources["MenuFont"];
            
            #region Menu's
            #region Main Menu
            Menu main = new Menu();
            var playbutton = new Button()
            {
                Font = Font,
                Text = "Play",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };

            playbutton.onClick = (x, e) => ButtonEvents.PlayButtononClick(x as Button, e);
            playbutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            playbutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            var optionsbutton = new Button() 
            {
                Font = Font,
                Text = "Options",
                TextColor = Color.Red,//TODO: Enable Button when options menu is implemented
                Position = Vector2.Zero
            };

            optionsbutton.onClick = (x, e) => ButtonEvents.OptionButtonsonClick(x as Button, e);
            optionsbutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            optionsbutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            var exitbutton = new Button()
            {
                Font = Font,
                Text = "Exit",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            exitbutton.onClick = (x, e) => ButtonEvents.ExitButtononClick(x as Button, e);
            exitbutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            exitbutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);

            main.AddButton(new Button[] { playbutton, optionsbutton, exitbutton });
            main.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            MenuHandle.RegisterMenu("main", main);
            #endregion
            #region Play Menu
            Menu PlayMenu = new Menu();

            var _1v1button = new Button()
            {
                Font = Font,
                Text = "1 Versus 1",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            _1v1button.onClick = (x, e) => ButtonEvents._1vs1ButtonClick(x as Button, e);
            _1v1button.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            _1v1button.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            var _2v2button = new Button()
            {
                Font = Font,
                Text = "2 Versus 2",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            _2v2button.onClick = (x, e) => ButtonEvents._2vs2ButtonClick(x as Button, e);
            _2v2button.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            _2v2button.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            var _3v3button = new Button()
            {
                Font = Font,
                Text = "3 Versus 3",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            _3v3button.onClick = (x, e) => ButtonEvents._3vs3ButtonClick(x as Button, e);
            _3v3button.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            _3v3button.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);

            PlayMenu.AddButton(new Button[] { _1v1button, _2v2button, _3v3button });
            PlayMenu.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            MenuHandle.RegisterMenu("playmenu", PlayMenu);
            #endregion
            #region Wait Menu
            Menu wait = new Menu();
            var waitbutton = new Button()
            {
                Font = Font,
                Text = "Waiting for other players.....",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            //waitbutton.onClick = (x, e) => { MenuHandle.ShowMenu("characterselect"); };      //ButtonEvents.WaitButtonClick(x as Button, e);
            wait.AddButton(waitbutton);
            wait.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            MenuHandle.RegisterMenu("waitmenu", wait);
            #endregion
            #region Character Selection
            //TODO: Hook Healer & Warrior once implemented
            Menu characterselect = new Menu();
            var magebutton = new Button()
            {
                Font = Font,
                Text = "The Mage",
                TextColor = Color.Black,
                Position = Vector2.Zero
            };
            magebutton.onClick = (x, e) => ButtonEvents.MageButtonClick(x as Button, e);
            magebutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            magebutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            var warriorbutton = new Button()
            {
                Font = Font,
                Text = "The Warrior",
                TextColor = Color.Red,
                Position = Vector2.Zero
            };
            /*
            warriorbutton.onClick = (x, e) => ButtonEvents.WarriorButtonClick(x as Button, e);
            warriorbutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            warriorbutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
             */
            var healerbutton = new Button()
            {
                Font = Font,
                Text = "The Healer",
                TextColor = Color.Black ,
                Position = Vector2.Zero
            };
            healerbutton.onClick = (x, e) => ButtonEvents.HealerButtonClick(x as Button, e);
            healerbutton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            healerbutton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);

            characterselect.AddButton(new Button[] { magebutton, warriorbutton, healerbutton });
            characterselect.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            
            MenuHandle.RegisterMenu("characterselect", characterselect);

            #endregion
            KeyBindMenu keybindMenu = new KeyBindMenu();
            var text = new Button() { Font = Font, TextColor = Color.Black, Text = "Press a key to bind" };
            keybindMenu.AddButton(text);
            keybindMenu.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            MenuHandle.RegisterMenu("keybindmenu", keybindMenu);

            #region OptionsMenu
            Menu OptionsMenu = new Menu();
            foreach (KeyValuePair<object, Keys> x in GameOptions.Controls)
            {
                var button = new Button() { Font = Font, Position = Vector2.Zero, TextColor = Color.Black, Text = x.Key.ToString() + ": " + x.Value.ToString() };
                button.onMouseEnter = (z, e) => ButtonEvents.onMouseEnter(z as Button, e);
                button.onMouseLeave = (z, e) => ButtonEvents.onMouseLeave(z as Button, e);
                button.onClick = (z, e) =>
                    {
                        keybindMenu.SetKeyBindMenu(z as Button, (z as Button).Text.Split(':').First());
                    };
                OptionsMenu.AddButton(button);
            }


            var ApplyButton = new Button() { Font = Font, TextColor = Color.Black, Text = "Apply" };
            ApplyButton.onClick = (x, e) => ButtonEvents.ApplyButtonClick(x as Button, e);
            ApplyButton.onMouseEnter = (x, e) => ButtonEvents.onMouseEnter(x as Button, e);
            ApplyButton.onMouseLeave = (x, e) => ButtonEvents.onMouseLeave(x as Button, e);
            OptionsMenu.AddButton(ApplyButton);

            OptionsMenu.Centralize(15, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth);
            MenuHandle.RegisterMenu("options", OptionsMenu);



            #endregion
            MenuHandle.ShowMenu("main");
            //Utilities.DrawSprite(new Sprite((Texture2D)ArenaGame.GameRecources["SwordSheet"], 12, 3, 500), new Vector2(200, 200),100); 
            /*
             * TODO: Warrior A Abilty (SERIALIZING SPRITE?) (HIGH FRAMERATE!)
             * CREATE Sprite WITH ENGINE ACTION (SERIALIZING NETMESSAGE ?)
             * ADD MODULES MAX RADIANS !
            */

            EngineAction Action = new EngineAction();
            //Action.onUpdate = (GameTime time) => { r }; 
           /* float rotatie = 0.0f;
            Action.onDraw = ( SpriteBatch batch, GameTime time) => {
                var Texture = (Texture2D)ArenaGame.GameRecources["Sword"];
                batch.Draw(Texture , new Vector2(200, 200), null, Color.White, rotatie,new Vector2(Texture.Width / 2 , Texture.Height), 1f, SpriteEffects.None, 0.5f);
                rotatie += 0.2f;
            };
            Action.Time = EngineAction.InfiniteTime;
            ActionHandler.AddAction(Action);
            */
            #endregion

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            MasterClient.Stop(true);
            if (GameClient != null) GameClient.Stop(true);
            //ForceKill XNA
            Process.GetCurrentProcess().Kill();

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.Escape)) Process.GetCurrentProcess().Kill();

            ActionHandler.Update(gameTime);
            if (MenuHandle.MenuVisible) MenuHandle.Update();
            else
            {
                
                #region Net Collision
                for(int i = 0; i < Projectile.LocalProjectiles.Count;i++)// (Projectile proj in Projectile.LocalProjectiles)
                {
                    for(int j = 0 ; j < NetPlayers.Count; j++)
                    {
                        Player ply = NetPlayers[j];
                        if (ply != null)
                        {

                            try
                            {
                                if (Projectile.LocalProjectiles[i] == null) break;
                                if (Collision.Collide(Projectile.LocalProjectiles[i], ply)) Projectile.LocalProjectiles[i].OnCollide(ply);
                            }
                            catch { }
                        }
                    }
                }
                for (int i = 0; i < Field.LocalFields.Count; i++)
                {
                    for(int j = 0 ; j < NetPlayers.Count; j++)
                    {
                        Player ply = NetPlayers[j];
                        if (ply != null)
                        {
                            try
                            {

                                if (Field.LocalFields[i] == null) break;
                                if (Collision.Collide(Field.LocalFields[i], ply)) Field.LocalFields[i].OnCollide(ply);
                            }
                            catch { }
                        }
                    }
                }
                #endregion
                GameObject.UpdateAll(gameTime);
            }


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (MenuHandle.MenuVisible) GraphicsDevice.Clear(Color.Gray);
            else GraphicsDevice.Clear(Color.White);
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            if (!MenuHandle.MenuVisible) spriteBatch.Draw((Texture2D)GameRecources["background"], new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
            ActionHandler.Draw(spriteBatch, gameTime);
            if (MenuHandle.MenuVisible) MenuHandle.Draw(spriteBatch);
            else
            {
                for (int i = 0; i < NetPlayers.Count; i++)
                {
                    NetPlayers[i].Draw(spriteBatch, gameTime);
                }
                for (int i = 0; i < NetObjects.Count; i++)
                {
                    NetObjects[i].Draw(spriteBatch, gameTime);
                }
                if(GUI != null) GUI.Draw(spriteBatch, gameTime);
                GameObject.DrawAll(gameTime, spriteBatch);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void GetAllFiles(string dir, ref List<string> Files)
        {
            foreach (string file in Directory.GetFiles(dir))
            {
                if (file.EndsWith(".xnb")) Files.Add(file);
             
            }
            foreach (string subdir in Directory.GetDirectories(dir))
            {
                GetAllFiles(subdir, ref Files);
            }
        }
        private void MasterDataRecieved(object Sender,OnRecieve args)
        {
            switch (args.Type)
            {
                case MessageType.MACTHMAKING:
                    {
                        var Message = Client.Deserialize<NetMessage>(args.Data);
                        GameClient = new Client(IP, (int)Message.Data[0]);
                        GameClient.DataRecieved += new EventHandler<OnRecieve>(GameDataRecieved);
                        GameClient.Start();
                        Log logger = new Log(ArenaGame.LogFile);
                        logger.Write("Starting Gameclient @ " + IP + ":" + Message.Data[0]);
                        MenuHandle.ShowMenu("characterselect");
                        GameClient.Send<NetMessage>(MessageType.HALLO, new NetMessage());
                        SoundPlayer.PlaySoundEffect((SoundEffect)ArenaGame.GameRecources["matchmakingqueued"]);
                    }
                    break;
            }
        }
        private void GameDataRecieved(object Sender, OnRecieve args)
        {
            switch (args.Type)
            {
                case MessageType.CHARACTERSELECT:
                    {
                        var Message = Client.Deserialize<NetMessage>(args.Data);
                        string choise = "";
                        switch ((int)Message.Data[1])
                        {
                            case 100: choise = "Healer"; break;
                            case 101: choise = "Mage"; break;
                            case 102: choise = "Warrior"; break;
                        }
                        var Size = ((SpriteFont)GameRecources["MenuFont"]).MeasureString(Message.Data[0].ToString() +  " has chosen " + choise);
                        Utilities.DrawMessage(Message.Data[0].ToString() + " has chosen " + choise, (SpriteFont)GameRecources["MenuFont"], new Vector2(GameOptions.GameWidth / 2 - Size.X / 2, Size.Y), Color.Red, 5);
                    }
                    break;
                case MessageType.GAMESTART:
                    {
                        MenuHandle.CurrentMenu.Hide();
                        
                        SoundPlayer.PlaySoundEffect((SoundEffect)GameRecources["play"]);
                        SoundPlayer.PlaySong((Song)GameRecources["BackMusic"], true);
                        SoundPlayer.Volume = 0.25f;
                        Me.Team = (Team)Client.Deserialize<NetMessage>(args.Data).Data.First();
                        Me.Reset();
                        var Size = ((SpriteFont)GameRecources["MenuFont"]).MeasureString("You are on " + Enum.GetName(typeof(Team), Me.Team));
                        Utilities.DrawMessage("You are on " + Enum.GetName(typeof(Team), Me.Team), (SpriteFont)GameRecources["MenuFont"], new Vector2(GameOptions.GameWidth / 2 - Size.X / 2, Size.Y * 2), Color.Red, 5);                  
                        GameClient.Send<Player>(MessageType.GAMESTART, Me);
                        break;
                    }
                case MessageType.POSITION:
                    {
                        var player = Client.Deserialize<Player>(args.Data);
                        if (NetPlayers.Contains(player))
                            NetPlayers[NetPlayers.FindIndex(x => x.Equals( player))] = player;
                        else
                            NetPlayers.Add(player);
                    }
                    break;
                case MessageType.DEATH:
                    {
                        var player = Client.Deserialize<Player>(args.Data);
                        NetPlayers[NetPlayers.FindIndex(x => x.Equals(player))] = player;
                        break;
                    }
                case MessageType.ABILITY_PROJECTILE :
                    {
                        var projectile = Client.Deserialize<Projectile>(args.Data);
                        if (NetObjects.Contains(projectile))
                            NetObjects[NetObjects.FindIndex(x => x.Equals(projectile))] = projectile;
                        else NetObjects.Add(projectile);
                    }
                    break;
                case MessageType.ABILITY_FIELD:
                    {
                        var field = Client.Deserialize<Field>(args.Data);
                        if (NetObjects.Contains(field))
                            NetObjects[NetObjects.FindIndex(x => x.Equals(field))] = field;
                        else NetObjects.Add(field);
                    }
                    break;
                case MessageType.FIELD_DESTROYED:
                    {
                        var field = Client.Deserialize<Field>(args.Data);
                        NetObjects.RemoveAll(x => (x as Field) != null && (x as Field).NetID == field.NetID);
                    }
                    break;
                case MessageType.PROJECTILE_DESTROYED:
                    {
                        var projectile = Client.Deserialize<Projectile>(args.Data);
                        NetObjects.RemoveAll(x => (x as Projectile) != null && (x as Projectile).NetID == projectile.NetID);
                    }
                    break;
                case MessageType.PROJECTILE_HIT:
                    {
                        var message = Client.Deserialize<NetMessage>(args.Data);
                        if (message.Data.First().ToString() == Username)
                        {
                            var proj = (Projectile)message.Data.Last();
                            Me.Damage(proj.Damage);
                            if (proj.Effect != null) proj.Effect.Apply(Me);
                         
                        }
                    }
                    break;
                case MessageType.ROUNDOVER:
                    {
                        var message = Client.Deserialize<NetMessage>(args.Data);
                        Team WinningTeam = (Team)message.Data.First() == Team.Blue ? Team.Red : Team.Blue;
                        var Size = ((SpriteFont)GameRecources["MenuFont"]).MeasureString(Enum.GetName(typeof(Team), WinningTeam) + " team has won the round!");
                        Utilities.DrawMessage(Enum.GetName(typeof(Team), WinningTeam) + " team has won the round!", (SpriteFont)GameRecources["MenuFont"], new Vector2(GameOptions.GameWidth / 2 - Size.X / 2, Size.Y), Color.Red, 5);
                        GUI.SetRound(WinningTeam);
 
                        EngineAction action = new EngineAction() { Time = 5 };
                        action.onUpdate = (gametime) =>
                        {
                            for (int i = 0; i < Projectile.LocalProjectiles.Count; i++)
                            {
                                if (Projectile.LocalProjectiles[i] != null) Projectile.LocalProjectiles[i].Destroy();
                            }
                            for (int i = 0; i < Field.LocalFields.Count; i++)
                            {
                                if (Field.LocalFields[i] != null) Field.LocalFields[i].Destroy();
                            }
                        };
                        action.onActionDone = (sender, ev) =>
                        {
                            Me.Reset();
                            GameClient.Send<Player>(MessageType.POSITION, Me);
                        };
                        ActionHandler.AddAction(action);
                     
                    }
                    break;
                case MessageType.GAMEDONE:
                    {
                         var message = Client.Deserialize<NetMessage>(args.Data);
                         Team WinningTeam = (Team)message.Data.First();
                         var Size = ((SpriteFont)GameRecources["MenuFont"]).MeasureString(Enum.GetName(typeof(Team), WinningTeam) + " team has won the round!");
                         Utilities.DrawMessage(Enum.GetName(typeof(Team), WinningTeam) + " team has won the game!", (SpriteFont)GameRecources["MenuFont"], new Vector2(GameOptions.GameWidth / 2 - Size.X / 2, Size.Y ), Color.Red, 5);
                         GUI.SetRound(WinningTeam);
                         EngineAction action = new EngineAction() { Time = 5 };
                         action.onActionDone = (sender, ev) =>
                         {
                             Me.Reset();
                             SoundPlayer.Stop();
                             MenuHandle.ShowMenu("main");
                             Projectile.LocalProjectiles.Clear();
                             NetPlayers.Clear();
                             NetObjects.Clear();
                             Me.Destroy();
                             GUI.ResetRounds();
                            
                         };
                         ActionHandler.AddAction(action);
                         
                    }
                    break;
                case MessageType.APPLY_EFFECT:
                    {
                        var Message = Client.Deserialize<NetMessage>(args.Data);
                        if (Message.Data.First().ToString() == Me.Username)
                        {
                            (Message.Data.Last() as Players.Ability.Effect).Apply(Me);
                        }
                    }
                    break;
            }
        }
    }
}
