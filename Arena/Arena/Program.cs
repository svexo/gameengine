using System;
using Arena.Login;
using System.Windows.Forms;
using System.Linq;
using GameEngine.Base;
namespace Arena
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {


            if (args.Length > 0)
            {
                Log x = new Log(ArenaGame.LogFile);
                string[] argmuments = args[0].Split('-');
                 foreach(string arg in argmuments)
                 {
                     if (arg.StartsWith("ip"))
                     {
                         ArenaGame.IP = arg.Split(':').Last();
                         x.Write("Setting serer IP to: " + ArenaGame.IP);
                     }
                 }
               
               
            }
            string Username;
            LoginScreen loginscr = new LoginScreen();
            loginscr.ShowDialog();
            if (!loginscr.IsDisposed)
            {
                loginscr.Dispose(out Username);
                using (ArenaGame game = new ArenaGame(Username))
                {
                    game.Run();
                }
            }
        }
    }
#endif
}

