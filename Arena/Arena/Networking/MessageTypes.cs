﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arena.Networking
{
    public struct MessageType
    {
        public const byte LOGIN = 100;
        public const byte MACTHMAKING = 101;
        public const byte CHARACTERSELECT = 102;
        public const byte GAMESTART = 103;
        public const byte ABILITY_PROJECTILE = 104;
        public const byte ABILITY_FIELD = 105;
        public const byte POSITION = 106;
        public const byte DEATH = 107;
        public const byte ROUNDOVER = 108;
        public const byte GAMEDONE = 109;
        public const byte MESSAGE = 110;
        public const byte HALLO = 111;
        public const byte PROJECTILE_DESTROYED = 112;
        public const byte PROJECTILE_HIT = 113;
        public const byte FIELD_HIT = 114;
        public const byte FIELD_DESTROYED = 115;
        public const byte APPLY_EFFECT = 116;
    }
}
