﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GameEngine.Networking;
using System.Security.Cryptography;
using GameEngine.Base;

namespace Arena.Login
{
    public partial class LoginScreen : Form
    {
        Log logger = new Log(ArenaGame.LogFile);
       
        public LoginScreen()
        {
            InitializeComponent();
        }
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
               
                ArenaGame.MasterClient = new Client(ArenaGame.IP, 7000);
                ArenaGame.MasterClient.Start(); 
                logger.Write("Starting MasterClient @ " + ArenaGame.IP + ":7000");

                ArenaGame.MasterClient.DataRecieved += new EventHandler<OnRecieve>(DataRecieved);
                TimeOut.Interval = 6000;
            }
            catch (Exception ex) { new ErrorScreen(ex).ShowDialog(); logger.Write("Excpetion@Login_Load:"  + e.ToString()); }
        }
        private void DataRecieved(object sender, OnRecieve args)
        {
            if (args.Type == 100)
            {
                this.Invoke(new Action(() =>
                {
                    LoginButton.Enabled = true;
                    UsernameBox.Enabled = true;
                    PasswordBox.Enabled = true;
                    TimeOut.Stop();
                }));

                var Message = Client.Deserialize<NetMessage>(args.Data);
                if ((bool)Message.Data[0])
                {
                    ArenaGame.MasterClient.DataRecieved = null;
                    this.Invoke(new Action(() => this.Close()));
   
                }
                else
                {
                    new ErrorScreen("Invalid username or password! ").ShowDialog();

                }
            }
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            //BYPASS
            //TODO: Disable bypass
           // ArenaGame.MasterClient.DataRecieved = null;
           // this.Close();
            //
            MethodInvoker Invoke = new MethodInvoker(Send);
            Invoke.Invoke();
            LoginButton.Enabled = false;
            UsernameBox.Enabled = false;
            PasswordBox.Enabled = false;
            screen = null;
            TimeOut.Start();
        }
        private void Send()
        {
            try
            {
                ArenaGame.MasterClient.Send<NetMessage>(100, new NetMessage() { Data = new object[] { UsernameBox.Text, GenerateMD5(PasswordBox.Text) } });
            }
            catch (Exception e)
            {
                new ErrorScreen(e).ShowDialog();
                logger.Write("Exception@SendingAuth: " + e.ToString());
            }
        }
        string GenerateMD5(string Password)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(Password);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
  
        }
        public void Dispose(out string Username)
        {
            Username = UsernameBox.Text;
            base.Dispose();
        }
        private void CancelButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) PasswordBox.Select();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            LoginButton.FlatAppearance.BorderSize = 1;
            LoginButton.FlatAppearance.BorderColor = Color.Blue;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            LoginButton.FlatAppearance.BorderSize = 0;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            CancelButton.FlatAppearance.BorderSize = 1;
            CancelButton.FlatAppearance.BorderColor = Color.Blue;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            CancelButton.FlatAppearance.BorderSize = 0;
        }
        ErrorScreen screen = null;
        private void TimeOut_Tick(object sender, EventArgs e)
        {
            LoginButton.Enabled = true;
            UsernameBox.Enabled = true;
            PasswordBox.Enabled = true;
            if(screen == null)
            {
                screen = new ErrorScreen("Cannot reach the server! (Timeout)");
                screen.ShowDialog();
                TimeOut.Stop();
            }
        }

        private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoginButton_Click(null, null);
            }
        }





    }
}
