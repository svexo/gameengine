﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using GameEngine.Base;
using Arena.Players.Ability;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameEngine.Physics;
using Arena.Networking;
using System.Diagnostics;
using System.Threading;

namespace Arena.Players
{
    public enum Team
    {
        Red,
        Blue,
        None
    }
    [Serializable()]
    public abstract  class Player : DynamicObject ,ISerializable
    {
        public const int NameOffset = 5;
        public const int SendInfoInterval = 1;
        public const float DefaultSpeed = 2f;

        public string Username { get; set; }
        public float HealthPoints { get; private set; }
        public Team Team { get; set; }
        private Vector2 PreviousPosition = Vector2.Zero;
        public virtual bool AbilityA_Cooldown { get; private set; }
        public virtual bool AbilityB_Cooldown { get; private set; }
        public virtual bool AbilityC_Cooldown { get; private set; }
        public virtual bool AbilityD_Cooldown { get; private set; }

        public string AbilityA_TextureName { get; protected set; }
        public string AbilityB_TextureName { get; protected set; }
        public string AbilityC_TextureName { get; protected set; }
        public string AbilityD_TextureName { get; protected set; }

        public override float layerDepth { get { return 0.3f; } }

        public bool Player_Death { get; private set; }

        public Player()  :base(true)
        {
            this.Speed = DefaultSpeed;
            Player_Death = false;
            this.HealthPoints = 100;
        }
        public virtual void Damage(float damage)
        {
            this.HealthPoints = this.HealthPoints - damage >= 0 ? this.HealthPoints - damage : 0;
            if (this.HealthPoints <= 0)
            {
                //TODO: OnDeath
                this.Rotation = 0f;
                this.Texture = (Texture2D)ArenaGame.GameRecources["skull"];
                Player_Death = true;
                ArenaGame.GameClient.Send<Player>(Arena.Networking.MessageType.DEATH, this);
            }
        }

        public virtual void Heal(float hp)
        {
            this.HealthPoints = HealthPoints + hp < 100 ? HealthPoints + hp : 100;
        }
        public virtual void Reset()
        {
            this.HealthPoints = 100;
            this.Speed = DefaultSpeed;
            this.Player_Death = false;
            if (this.Team == Players.Team.Blue) this.Position = ArenaGame.BlueSpawn;
            else this.Position = ArenaGame.RedSpawn;

            AbilityA_Cooldown = false;
            AbilityB_Cooldown = false;
            AbilityC_Cooldown = false;
            AbilityD_Cooldown = false;
        }
        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, Microsoft.Xna.Framework.GameTime gameTime)
        {
            Color color = this.Team == Players.Team.Blue ? Color.Blue : Color.Red;
            Vector2 FontSize = ((SpriteFont)ArenaGame.GameRecources["NameFont"]).MeasureString(Username);
            spriteBatch.DrawString((SpriteFont)ArenaGame.GameRecources["NameFont"], Username,
                new Vector2(this.Position.X - this.Width , this.Position.Y - FontSize.Y - NameOffset ),
                color, 0f, Vector2.Zero, 1f, SpriteEffects.None, this.layerDepth);

            spriteBatch.Draw(Texture, Position, null, Color.White, Rotation, Origin, 1f, SpriteEffects.None, this.layerDepth);
        }
        public override bool Equals(object obj)
        {
            if (obj as Player != null)
            {
                if (((Player)obj).Username == this.Username)
                {
                    return true;
                }
            }
           return false;
        }
        public override int GetHashCode()
        {
            return Username.Length;
        }
        public override void Update(GameTime time)
        {
            if (Player_Death) return;
            
            Input();
            if (PreviousPosition != Position)
            {
                PreviousPosition = Position;
                 if ((((int)time.TotalGameTime.TotalMilliseconds) % SendInfoInterval) == 0)
                {
                    Action action = delegate { ArenaGame.GameClient.Send<Player>(MessageType.POSITION, this); };
                    action.Invoke();
                }
             
            }
            base.Update(time);
           
        }
        private void Input()
        {
            KeyboardState Kstate = Keyboard.GetState();
            MouseState MState = Mouse.GetState();

            this.Rotation = Physics.CalculateRotation(this.Position, new Vector2(MState.X, MState.Y));
            Vector2 Movem = new Vector2();

            if (Kstate.IsKeyDown(GameOptions.Controls["up"])) Movem.Y = - 1f;
            if (Kstate.IsKeyDown(GameOptions.Controls["down"])) Movem.Y = 1f;
            if (Kstate.IsKeyDown(GameOptions.Controls["left"])) Movem.X = -1f;
            if (Kstate.IsKeyDown(GameOptions.Controls["right"])) Movem.X = 1f;
            if (Kstate.IsKeyDown(GameOptions.Controls["abilitya"])) AbilityA(new Vector2(MState.X, MState.Y));
            if (Kstate.IsKeyDown(GameOptions.Controls["abilityb"])) AbilityB(new Vector2(MState.X, MState.Y));
            if (Kstate.IsKeyDown(GameOptions.Controls["abilityc"])) AbilityC(new Vector2(MState.X, MState.Y));
            if (Kstate.IsKeyDown(GameOptions.Controls["abilityd"])) AbilityD(new Vector2(MState.X, MState.Y));
            this.Velocity = Movem;

            if(Kstate.IsKeyUp(GameOptions.Controls["up"]) && Kstate.IsKeyUp(GameOptions.Controls["down"]) &&
                Kstate.IsKeyUp(GameOptions.Controls["left"]) && Kstate.IsKeyUp(GameOptions.Controls["right"]))
            {
                this.Velocity = Vector2.Zero;
            }
        }
        public virtual void AbilityA(Vector2 Target) { }
        public virtual void AbilityB(Vector2 Target) { }
        public virtual void AbilityC(Vector2 Target) { }
        public virtual void AbilityD(Vector2 Target) { }

        public override void OnCollide(GameObject Object)
        {
            if (Object as Field != null) return;
            this.Position -= this.Velocity;
            //base.OnCollide(Object);
        }
      
        public Player(SerializationInfo info, StreamingContext context)
        {
            this.Username = (string)info.GetValue("username", typeof(string));
            Debug.WriteLine(info.GetValue("texture", typeof(string)));
            if(ArenaGame.GameRecources != null && ArenaGame.GameRecources.Count > 0)
            {
                this.Texture = (Texture2D)ArenaGame.GameRecources[(string)info.GetValue("texture", typeof(string))];
            }
            this.Position = (Vector2)info.GetValue("position", typeof(Vector2));
            this.HealthPoints = (float)info.GetValue("health", typeof(float));
            this.Team = (Team)info.GetValue("team", typeof(Team));
            this.Visible = (bool)info.GetValue("visible", typeof(bool));
            this.Rotation = (float)info.GetValue("rotation", typeof(float));
            this.AplhaCollision = (int)info.GetValue("alphac", typeof(int));
            this.Player_Death = (bool)info.GetValue("death", typeof(bool));
        }
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("username", this.Username);
            info.AddValue("texture",this.Texture.Name);
            info.AddValue("position",this.Position);
            info.AddValue("health", this.HealthPoints);
            info.AddValue("team", this.Team);
            info.AddValue("visible", this.Visible);
            info.AddValue("rotation", this.Rotation);
            info.AddValue("alphac", this.AplhaCollision);
            info.AddValue("death", Player_Death);
        }


    }
}
