﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arena.Players.Ability;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Runtime.Serialization;
using System.Windows.Forms;
using GameEngine.Base;
namespace Arena.Players.Characters
{
    #region Abilities Parameters
    struct MageAbility
    {
        public const int A_Damage = 10;
        public const int A_LifeTime = 10;
        public const int A_Speed = 5;
        public const int A_Cooldown = 1000;

        public const int B_Damage = 50;
        public const int B_LifeTime = 10;
        public const int B_Speed = 2;
        public const int B_Cooldown = 8000;

        public const int C_Damage = 50;
        public const int C_LifeTime = 10;
        public const float C_Slow = 60f;
        public const int C_SlowTime = 4;
        public const int C_Speed = 5;
        public const int C_Cooldown = 5000;

        public const int D_InvulnerableTime = 4;
        public const int D_Cooldown = 40000;

    }
    #endregion

    [Serializable()]
    public class Mage : Player
    {
        private Timer ATimer = new Timer();
        private Timer BTimer = new Timer();
        private Timer CTimer = new Timer();
        private Timer DTimer = new Timer();

        public override bool AbilityA_Cooldown { get { return ATimer.Enabled; } }
        public override bool AbilityB_Cooldown { get { return BTimer.Enabled; } }
        public override bool AbilityC_Cooldown { get { return CTimer.Enabled; } }
        public override bool AbilityD_Cooldown { get { return DTimer.Enabled; } }

        private bool Invunarable = false;

        void Timer_Tick(object sender, EventArgs e)
        {
            ((Timer)sender).Stop();
        }

        public Mage()
            : base()
        {
            Texture = (Texture2D)ArenaGame.GameRecources["Mage"];
            this.AbilityA_TextureName = "comet";
            this.AbilityB_TextureName = "firewave";
            this.AbilityC_TextureName = "FrostBite";
            this.AbilityD_TextureName = "invunarable";
            Position = new Vector2(202, 22);
          
            #region InitTimers
            ATimer.Interval = MageAbility.A_Cooldown;
            ATimer.Tick += new EventHandler(Timer_Tick);
            BTimer.Interval = MageAbility.B_Cooldown;
            BTimer.Tick += new EventHandler(Timer_Tick);
            CTimer.Interval = MageAbility.C_Cooldown;
            CTimer.Tick += new EventHandler(Timer_Tick);
            DTimer.Interval = MageAbility.D_Cooldown;
            DTimer.Tick += new EventHandler(Timer_Tick);
            #endregion
        }
        public override void Reset()
        {
            base.Reset();
            ATimer.Stop();
            BTimer.Stop();
            CTimer.Stop();
            DTimer.Stop();
            this.Texture = (Texture2D)ArenaGame.GameRecources["Mage"];

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        public Mage(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public override void AbilityA(Vector2 Target)
        {
            if (!ATimer.Enabled)
            {
                Projectile x = new Projectile(MageAbility.A_Damage, this, MageAbility.A_LifeTime, (Texture2D)ArenaGame.GameRecources[this.AbilityA_TextureName]);
                x.Velocity = x.CalculateDirection(Target);
                x.Speed = MageAbility.A_Speed;
                ATimer.Start();
            }
        }
        public override void AbilityB(Vector2 Target)
        {
            if (!BTimer.Enabled)
            {
                Projectile x = new Projectile(MageAbility.B_Damage, this, MageAbility.B_LifeTime, (Texture2D)ArenaGame.GameRecources[this.AbilityB_TextureName]);
                x.Velocity = x.CalculateDirection(Target);
                x.Speed = MageAbility.B_Speed;
                BTimer.Start();
            }
        }
        public override void AbilityC(Vector2 Target)
        {
            if (!CTimer.Enabled)
            {
                Projectile x = new Projectile(MageAbility.C_Damage, this, MageAbility.C_LifeTime, (Texture2D)ArenaGame.GameRecources[this.AbilityC_TextureName]);
                x.Velocity = x.CalculateDirection(Target);
                x.Speed = MageAbility.C_Speed;
                x.Effect = new Ability.Effect() { Intensity = MageAbility.C_Slow, Time = MageAbility.C_SlowTime, Type = Ability.Effect.EffectType.Slow };
                CTimer.Start();
            }
        }
        public override void AbilityD(Vector2 Target)
        {
            if (!DTimer.Enabled)
            {
                //TODO: Change mage texture
                EngineAction action = new EngineAction();
                Invunarable = true;
                action.onActionDone += (sender, args) => { Texture = (Texture2D)ArenaGame.GameRecources["Mage"]; Invunarable = false; };
                action.Time = MageAbility.D_InvulnerableTime;
                ActionHandler.AddAction(action);
                DTimer.Start();
            }
        }
        public override void Damage(float damage)
        {
            if(!Invunarable)
                base.Damage(damage);
        }
    }
}