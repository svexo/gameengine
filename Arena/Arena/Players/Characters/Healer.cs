﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arena.Players.Ability;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Runtime.Serialization;
using System.Windows.Forms;
using GameEngine.Base;
using Arena.Networking;
using GameEngine.Networking;
namespace Arena.Players.Characters
{
    #region Abilities Parameters
    struct HealerAbility
    {
        public const int A_Cooldown = 1000;
        public const int A_Damage = 5;
        public const int A_Lifetime = 5;
        public const int A_Speed = 5;

        public const int B_Cooldown = 5000;
        public const int B_Damage = 4;
        public const int B_Damage_Time = 5;
        public const int B_Heal = 5;
        public const int B_Heal_Time = 5;
        public const int B_LifeTime = 5;
      

        public const int C_Cooldown = 10000;

        public const int D_HealPrecent = 20;
        public const int D_Cooldown = 20000;
    }
    #endregion 
    [Serializable()]
    public class Healer : Player
    {
        private Timer ATimer = new Timer();
        private Timer BTimer = new Timer();
        private Timer CTimer = new Timer();
        private Timer DTimer = new Timer();

        public override bool AbilityA_Cooldown { get { return ATimer.Enabled; } }
        public override bool AbilityB_Cooldown { get { return BTimer.Enabled; } }
        public override bool AbilityC_Cooldown { get { return CTimer.Enabled; } }
        public override bool AbilityD_Cooldown { get { return DTimer.Enabled; } }

        void Timer_Tick(object sender, EventArgs e)
        {
            ((Timer)sender).Stop();
        }

        public Healer()
            : base()
        {
            Texture = (Texture2D)ArenaGame.GameRecources["Mage"];
            Position = new Vector2(202, 22);
            #region InitTimers
            ATimer.Interval = HealerAbility.A_Cooldown;
            ATimer.Tick += new EventHandler(Timer_Tick);
            BTimer.Interval = HealerAbility.B_Cooldown;
            BTimer.Tick += new EventHandler(Timer_Tick);
            CTimer.Interval = HealerAbility.C_Cooldown;
            CTimer.Tick += new EventHandler(Timer_Tick);
            DTimer.Interval = HealerAbility.D_Cooldown;
            DTimer.Tick += new EventHandler(Timer_Tick);
            #endregion
            this.AbilityA_TextureName = "shard";
            this.AbilityB_TextureName = "heal";
            this.AbilityC_TextureName = "teleport";
            this.AbilityD_TextureName = "teleport";
        }
        public Healer(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public override void Reset()
        {
            base.Reset();
            ATimer.Stop();
            BTimer.Stop();
            CTimer.Stop();
            DTimer.Stop();
            this.Texture = (Texture2D)ArenaGame.GameRecources["Mage"];

        }
        public override void AbilityA(Vector2 Target)
        {
            if (!ATimer.Enabled)
            {
                Projectile x = new Projectile(HealerAbility.A_Damage, this, HealerAbility.A_Lifetime, (Texture2D)ArenaGame.GameRecources[this.AbilityA_TextureName]);
                x.Velocity = x.CalculateDirection(Target);
                x.Speed = HealerAbility.A_Speed;
                ATimer.Start();
            }
        }
        public override void AbilityB(Vector2 Target)
        {
            if (!BTimer.Enabled)
            {
                //TODO: Add texture
                Field fld = new Field(this, HealerAbility.B_LifeTime, (Texture2D)ArenaGame.GameRecources[this.AbilityB_TextureName],
                    (x) =>
                    {
                        Players.Ability.Effect eff;
                        if (x.Team == this.Team)
                        {
                            eff = new Ability.Effect() { Type = Ability.Effect.EffectType.Heal, Time = HealerAbility.B_Heal_Time, Intensity = HealerAbility.B_Heal };
                        }
                        else
                        {
                            eff = new Ability.Effect() { Type = Ability.Effect.EffectType.Bleed, Time = HealerAbility.B_Damage_Time, Intensity = HealerAbility.B_Damage };
                        }
                        Action act = new Action(() =>
                        {
                            if (x.Username != ArenaGame.Me.Username)
                                ArenaGame.GameClient.Send<NetMessage>(MessageType.APPLY_EFFECT, new NetMessage() { Data = new object[] { x.Username, eff } });
                            else
                                eff.Apply(ArenaGame.Me);

                        }); act.Invoke();
                    }
                    , this.Position);

                BTimer.Start();
            }
        }
        public override void AbilityC(Vector2 Target)
        {
            if (!CTimer.Enabled)
            {

                CTimer.Start();
            }
        }
        public override void AbilityD(Vector2 Target)
        {
            if (!DTimer.Enabled)
            {

                this.Position = Target;
              //  this.Heal(this.HealthPoints / 100 * (100 - 20));
                DTimer.Start();
            }
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
