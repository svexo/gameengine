﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arena.Players.Ability;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Runtime.Serialization;
using System.Windows.Forms;
using GameEngine.Base;

namespace Arena.Players.Characters
{
    #region Abilities Parameters
    struct WarriorAbility
    {
        public const int A_Cooldown = 1500;
        public const int A_Range = 30;
        public const int A_Damage = 30;
        public const int B_Cooldown = 150000;

        public const int C_Cooldown = 1500000;

        public const int D_Cooldown = 1500000;
    }
    #endregion
      [Serializable()]
    public class Warrior : Player
    {
        private Timer ATimer = new Timer();
        private Timer BTimer = new Timer();
        private Timer CTimer = new Timer();
        private Timer DTimer = new Timer();

        public override bool AbilityA_Cooldown { get { return ATimer.Enabled; } }
        public override bool AbilityB_Cooldown { get { return BTimer.Enabled; } }
        public override bool AbilityC_Cooldown { get { return CTimer.Enabled; } }
        public override bool AbilityD_Cooldown { get { return DTimer.Enabled; } }

        void Timer_Tick(object sender, EventArgs e)
        {
            ((Timer)sender).Stop();
        }
        public Warrior()
            : base()
        {
            Texture = (Texture2D)ArenaGame.GameRecources["Warrior"];

            Position = new Vector2(202, 22);
            #region InitTimers
            ATimer.Interval = WarriorAbility.A_Cooldown;
            ATimer.Tick += new EventHandler(Timer_Tick);
            BTimer.Interval = WarriorAbility.B_Cooldown;
            BTimer.Tick += new EventHandler(Timer_Tick);
            CTimer.Interval = WarriorAbility.C_Cooldown;
            CTimer.Tick += new EventHandler(Timer_Tick);
            DTimer.Interval = WarriorAbility.D_Cooldown;
            DTimer.Tick += new EventHandler(Timer_Tick);
            #endregion
        }
        public override void AbilityA(Vector2 Target)
        {
            if (!ATimer.Enabled)
            {
                for (int i = 0; i < ArenaGame.NetPlayers.Count; i++)
                {
                    if (ArenaGame.NetPlayers[i].GetDistance(this) > 30)
                    {

                    }
                }
                ATimer.Start();
            }
        }
        public override void AbilityB(Vector2 Target)
        {
            if (!BTimer.Enabled)
            {

                BTimer.Start();
            }
        }
        public override void AbilityC(Vector2 Target)
        {
            if (!CTimer.Enabled)
            {

                CTimer.Start();
            }
        }
        public override void AbilityD(Vector2 Target)
        {
            if (!DTimer.Enabled)
            {

                DTimer.Start();
            }
        }
         
    }
}
