﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameEngine.Base;
using GameEngine.Physics;
using Arena.Networking;
using System.Runtime.Serialization;
using GameEngine.Networking;
using Arena.Players.Characters;

namespace Arena.Players.Ability
{
    [Serializable()]
    public class Projectile : DynamicObject , ISerializable
    {
        public static List<Projectile> LocalProjectiles = new List<Projectile>();
        public GameObject Parent { get; set; }
        public int LifeTime { get; set; }
        public Effect Effect { get; set; }
        public int Damage { get; set; }
        public const int SendInfoInterval = 1;
        public int NetID { get; private set; }
        public override float layerDepth { get { return 0.21f; } }
        public Projectile()
            : base(true)
        {
            
        }
        public Projectile(SerializationInfo info, StreamingContext context) 
        {
            this.Damage = (int)info.GetValue("damage", typeof(int));
            this.Texture = (Texture2D)ArenaGame.GameRecources[(string)info.GetValue("texture", typeof(string))];
            this.Position = (Vector2)info.GetValue("position", typeof(Vector2));
            this.NetID = (int)info.GetValue("netid", typeof(int));
            this.LifeTime = (int)info.GetValue("lifetime", typeof(int));
            this.Visible = (bool)info.GetValue("visible", typeof(bool));
            this.Rotation = (float)info.GetValue("rotation", typeof(float));
            this.AplhaCollision = (int)info.GetValue("alphac", typeof(int));
            this.Effect = (Effect)info.GetValue("effect", typeof(Effect));
       
        }
        public Projectile(int Damage, GameObject Parent, int LifeTime, Texture2D Texture)
            : base(true)
        {
            this.Texture = Texture;
            this.LifeTime = LifeTime;
            this.Parent = Parent;
            this.Damage = Damage;
            this.Position = Parent.Position;
            this.NetID = new Random().Next();
            LocalProjectiles.Add(this);
       
        }
        public Vector2 CalculateDirection(Vector2 Target)
        {
            this.Rotation = Physics.CalculateRotation(this.Position, Target);
            return Physics.CalculateDirection(Parent.Position, Target);
        }
        public override bool Equals(object obj)
        {
            if (obj as Projectile != null)
            {
                if (((Projectile)obj).NetID == NetID) return true;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return NetID;
        }
        public override void Update(Microsoft.Xna.Framework.GameTime time)
        {
            LifeTime -= time.ElapsedGameTime.Seconds;
            if (LifeTime <= 0) this.Destroy();
            if ((((int)time.TotalGameTime.TotalMilliseconds) % SendInfoInterval) == 0)
            {  
                Action action = delegate { ArenaGame.GameClient.Send<Projectile>(MessageType.ABILITY_PROJECTILE, this); };
                action.Invoke(); //Send Async
            }
            base.Update(time);
            if (this.Position.X < 0 || this.Position.X > GameOptions.GameWidth || this.Position.Y < 0 || this.Position.Y > GameOptions.GameHeight)
            {
                Destroy();
            }
        }
        public override void Destroy()
        {
            Action action = delegate { ArenaGame.GameClient.Send<Projectile>(MessageType.PROJECTILE_DESTROYED, this); };
            action.Invoke();
            LocalProjectiles.Remove(this);
            base.Destroy();
        }
        public override void OnCollide(GameObject Object)
        {
            if (Object != Parent && Object.GetType() != this.GetType())
            {
                if (typeof(Player).IsAssignableFrom(Object.GetType()))
                {
                    Player player = Object as Player;
                    Action action = delegate { ArenaGame.GameClient.Send<NetMessage>(MessageType.PROJECTILE_HIT, new NetMessage() { Data = new object[] { player.Username, this } }); };
                    action.Invoke(); 
                }
                if (Object as Field == null)
                {
                    this.Destroy();
                }
            }
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("texture", this.Texture.Name);
            info.AddValue("position", this.Position);
            info.AddValue("visible", this.Visible);
            info.AddValue("rotation", this.Rotation);
            info.AddValue("alphac", this.AplhaCollision);
            info.AddValue("damage", this.Damage);
            info.AddValue("lifetime", this.LifeTime);
            info.AddValue("netid",this.NetID);
            info.AddValue("effect", this.Effect);
            
        }
    }
}
