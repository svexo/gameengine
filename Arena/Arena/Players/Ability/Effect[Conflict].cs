﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Base;
using System.Runtime.Serialization;

namespace Arena.Players.Ability
{
    [Serializable()]
    public class Effect : ISerializable
    {
        public EffectType Type { get; set; }
        public int Time { get; set; }
        public float Intensity { get; set; }


        public Effect() { }
        public Effect(SerializationInfo info, StreamingContext context)
        {
            this.Type = (EffectType)info.GetValue("type", typeof(EffectType));
            this.Time = (int)info.GetValue("time", typeof(int));
            this.Intensity = (float)info.GetValue("intensity", typeof(float));

        }
        public enum EffectType
        {
            Heal,
            Slow,
            Bleed
        }
        public void Apply(Player player)
        {
            switch (Type)
            {
                case EffectType.Slow:
                    {
                        EngineAction Action = new EngineAction();
                        Action.Time = Time;
                        player.Speed = player.Speed -  (player.Speed / 100 * Intensity);
                        Action.onActionDone += new EventHandler((s, e) => { player.Speed = Player.DefaultSpeed; });
                        ActionHandler.AddAction(Action);
                    }
                    break;
            }
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("type", this.Type);
            info.AddValue("time", this.Time);
            info.AddValue("intensity", this.Intensity);
        }
    }
}
