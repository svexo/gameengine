﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Base;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;
using Arena.Networking;

namespace Arena.Players.Ability
{
    [Serializable()]
    public class Field : GameObject, ISerializable
    {
        public delegate void OnEnter(Player obj);
        public static List<Field> LocalFields = new List<Field>();
        public double LifeTime { get; set; }
        public GameObject Parent { get; set; }
        public Vector2 Target { get; set; }
        private OnEnter result;
        private List<Player> entered = new List<Player>();
        private Vector2 prevPos = Vector2.Zero;
        public int NetID { get; private set; }
        public override float layerDepth { get { return 0.2f; } }

        public Field(GameObject Parent, int LifeTime, Texture2D Texture, OnEnter result , Vector2 Position)
            : base(true)
        {
            this.Position = Position;
            this.result = result;
            this.LifeTime = LifeTime;
            this.Parent = Parent;
            this.Texture = Texture;
            this.NetID = new Random().Next();
            LocalFields.Add(this);
    
        }
        public Field() : base(true) { }
        public override bool Equals(object obj)
        {
            if (obj as Field != null)
            {
                if (((Field)obj).NetID == NetID) return true;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return NetID;
        }
        public override void Update(GameTime gameTime)
        {
            this.LifeTime -= gameTime.ElapsedGameTime.TotalSeconds;
            if (LifeTime <= 0) this.Destroy();

            if (prevPos != Position)
            {
                prevPos = Position;
                Action action = delegate { ArenaGame.GameClient.Send<Field>(MessageType.ABILITY_FIELD, this); };
                action.Invoke(); //Send Async
            } 
            base.Update(gameTime);
        }
        public override void Destroy()
        {
            Action action = delegate { ArenaGame.GameClient.Send<Field>(MessageType.FIELD_DESTROYED, this); };
            action.Invoke();
            LocalFields.Remove(this);
            base.Destroy();
        }
        public override void OnCollide(GameObject X)
        {
            if (typeof(Player).IsAssignableFrom(X.GetType()))
            {
                if (entered.Find( (ply) => ply.Username == ((Player)X).Username) == null)
                {
                    result(X as Player);
                    entered.Add(X as Player);
                } 
            }
        }

        public Field(SerializationInfo info, StreamingContext context)
        {
            this.Texture = (Texture2D)ArenaGame.GameRecources[(string)info.GetValue("texture", typeof(string))];
            this.Position = (Vector2)info.GetValue("position", typeof(Vector2));
            this.NetID = (int)info.GetValue("netid", typeof(int));
            this.LifeTime = (int)info.GetValue("lifetime", typeof(int));
            this.Visible = (bool)info.GetValue("visible", typeof(bool));
            this.Rotation = (float)info.GetValue("rotation", typeof(float));
            this.AplhaCollision = (int)info.GetValue("alphac", typeof(int));
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("texture", this.Texture.Name);
            info.AddValue("position", this.Position);
            info.AddValue("visible", this.Visible);
            info.AddValue("rotation", this.Rotation);
            info.AddValue("alphac", this.AplhaCollision);
            info.AddValue("lifetime", this.LifeTime);
            info.AddValue("netid", this.NetID);
        }
    }
}
