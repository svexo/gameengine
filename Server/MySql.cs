﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Extension;
namespace Server
{ 
    public class MySql : IDisposable
    {
        private MySqlConnection Connection;
        public void Connect(string Server, string Database, string User, string Password)
        {
            Connection = new MySqlConnection(string.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3};", Server, Database, User, Password));
            Connection.Open();
        }
        public bool ValidateUser(string Name, string Password)
        {
            MySqlDataReader rdr;
            string qry = string.Format("SELECT * FROM users WHERE Name='{0}'", Name);
            MySqlCommand cmd = new MySqlCommand(qry, Connection);
            rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

                bool result = Password == rdr.GetString(2);
                rdr.Close();
                return result;
            }
            rdr.Close();

            return false;
        }
        public bool AddUser(string User, string Password)
        {

            MySqlDataReader rdr;
            string qry = string.Format("SELECT * FROM users WHERE Name='{0}'", User);
            MySqlCommand cmd = new MySqlCommand(qry, Connection);
            rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                rdr.Close();
                return false;

            }
            rdr.Close();
            qry = string.Format("INSERT INTO `users`(`ID`, `Name`, `Password`) VALUES (NULL,'{0}','{1}')", User, Password);
            cmd = new MySqlCommand(qry, Connection);

            cmd.ExecuteNonQuery();
            return true;
        }
        public void ChangePassword(string User, string Password)
        {
     
        }



        public void Dispose()
        {
          
            Connection.Close();
        }

    }
} 

