﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using GameEngine.Networking;
using System.Threading;
using System.Collections;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Net.Sockets;
using Arena.Players;

namespace Server
{
 static class ServerHandler
    {
        public delegate void DataRecieved(Server server, byte type, byte[] data, IPEndPoint Sender);
        public static Dictionary<int, Server> Servers = new Dictionary<int, Server>();
       
        public static void Intialize(int StartPort,int MaxNumberOfServers,DataRecieved OnRecieve)
        {
            for (int i = StartPort; i < StartPort + MaxNumberOfServers; i++)
            {
                Servers.Add(i, new Server(i, OnRecieve));
            }
        }
        public static int StartServer()
        {
            
            foreach (Server server in Servers.Values)
            {
                if (!server.IsRunning)
                {
                    server.Start();
                    return server.Port;
                }
            }
            throw new NoServerAvailable();
       
        }
        public static void StopServer(int ServerPort,bool ForceKill = false)
        {
            Server ser = (Server)Servers[ServerPort];
            ser.Stop(ForceKill);
        }
    }
    public class NoServerAvailable : Exception
    {
        public NoServerAvailable() : base("No Servers Are Available") { }
    }
    class Server : Serialization
    {
        public enum GameData
        {
            Deaths,
            Round,
            CharacterSelected,
            Players
        }
        public int Port { get; private set; }
        public bool IsRunning { get; private set; }
        public List<IPEndPoint> Players = new List<IPEndPoint>();
        public Dictionary<GameData , object> Data = new Dictionary<GameData, object>();
        private ServerHandler.DataRecieved Callback;
        private Thread ServerThread;
        private UdpClient Network;
        public Server(int Port,ServerHandler.DataRecieved Handler)
        {
            this.Port = Port;
            this.Callback = Handler;
            Program.Print("Server@" + Port+ ":",ConsoleColor.Green);
            Console.WriteLine(" Initialized!");
            

        }
        public void Start()
        {
            IsRunning = true;
            this.Network = new UdpClient(Port);
            ServerThread = new Thread(new ThreadStart(Serverloop));
            ServerThread.Start();
            //Players.Clear();
            Data.Clear();
            Data.Add(GameData.Round, new List<Team>());
            Data.Add(GameData.Players,new List<Player>());
            Data.Add(GameData.CharacterSelected, 0);
            Program.Print("Server@" + Port + ":", ConsoleColor.Green);
            Console.WriteLine(" Started !");
        }
        public void Stop(bool ForceKill)
        {
            IsRunning = false;
            this.Network.Close();
            Program.Print("Server@" + Port + ":",ConsoleColor.Green);
            Console.WriteLine(" server standby");
            Players.Clear();
            if (ForceKill) ServerThread.Abort();
        }
        private void Serverloop()
        {
            IPEndPoint Sender =  new IPEndPoint(IPAddress.Any,this.Port);
            while (IsRunning)
            {
                try
                {
                    byte[] data = Network.Receive(ref Sender);
                    byte[] ObjData;
                    byte Type;
                    GetRequestType(data, out ObjData, out Type);
                    Callback(this, Type, ObjData, Sender);
                }
                catch (SocketException ex)
                {
                    if (!IsRunning) return;
                    IsRunning = false;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Server@" + Port);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" has been stopped because of a socket exception");
                    Console.WriteLine(ex.ToString());
                    Console.ForegroundColor = ConsoleColor.White;
                    Players.Clear();
                  
                    Network.Close();
                }
            }
        }
        public void Send(IPEndPoint Reciever, byte[] Data,byte Type)
        {
            Data = AddRequestType(Type,Data);
            Network.Send(Data, Data.Length, Reciever);
        }
        public void Send(IPEndPoint[] Recievers, byte[] Data,byte Type) { Parallel.ForEach(Recievers, reciever => Send(reciever, Data,Type)); }
        public void Send<T>(IPEndPoint Reciever, T Data,byte Type)where T : ISerializable
        {
            byte[] ObjData = Server.Serialize<T>(Data);
            ObjData = AddRequestType(Type, ObjData);
            Network.Send(ObjData, ObjData.Length,Reciever);
        }
        public void Send<T>(IPEndPoint[] Recievers, T Data,byte Type) where T : ISerializable { Parallel.ForEach(Recievers, reciever => Send(reciever, Data,Type)); }
        private byte[] AddRequestType(byte Type, byte[] Data)
        {
            byte[] data = new byte[Data.Length + 1];
            data[0] = Type;
            for (int i = 1; i < data.Length; i++)
            {
                data[i] = Data[i - 1];
            }
            return data;
        }
        private void GetRequestType(byte[] OrginalData, out byte[] ObjectData, out byte Type)
        {
            ObjectData = new byte[OrginalData.Length - 1];
            for (int i = 1; i < OrginalData.Length; i++)
            {
                ObjectData[i - 1] = OrginalData[i];
            }
            Type = OrginalData[0];
        }
    }
}
