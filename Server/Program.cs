﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Collections;
using GameEngine.Networking;
using Arena.Players;

namespace Server
{

    public struct MessageType
    {
        public const byte LOGIN = 100;
        public const byte MACTHMAKING = 101;
        public const byte CHARACTERSELECT = 102;
        public const byte GAMESTART = 103;
        public const byte ABILITY_PROJECTILE = 104;
        public const byte ABILITY_FIELD = 105;
        public const byte POSITION = 106;
        public const byte DEATH = 107;
        public const byte ROUNDOVER = 108;
        public const byte GAMEDONE = 109;
        public const byte MESSAGE = 110;
        public const byte HELLO = 111;
        public const byte PROJECTILE_DESTROYED = 112;
        public const byte PROJECTILE_HIT = 113;
        public const byte FIELD_HIT = 114;
        public const byte FIELD_DESTROYED = 115;
        public const byte APPLY_EFFECT = 116;
    }
    public struct MatchMaking
    {
        public const byte _1VS1 = 100;
        public const byte _2VS2 = 101;
        public const byte _3VS3 = 102;
    }
    class Program
    {
        static Server Master = new Server(7000, DataRecieved);
        static MySql Database = new MySql();
        static List<IPEndPoint> _1VS1Queue = new List<IPEndPoint>();
        static List<IPEndPoint> _2VS2Queue = new List<IPEndPoint>();
        static List<IPEndPoint> _3VS3Queue = new List<IPEndPoint>();
        static bool Running = true;

        static void Main(string[] args)
        {
            Console.Title = "Arena Server";
            try
            {

                Database.Connect("diede.apers.net", "Arena", "Svexo", "sverre12");
                PrintLine("Database connection OK!", ConsoleColor.Green);

            }
            catch {PrintLine("Database connection failed!",ConsoleColor.Red); }
 
            Console.WriteLine("Initializing Game Servers !");
            Master.Start();
            ServerHandler.Intialize(7001, 5, DataRecieved);
            while (Running)
            {
                if (Console.KeyAvailable)
                {
                    Console.Write("> ");
                    string input = Console.ReadLine();
                    if (input == "q")
                    {
                        foreach (Server serv in ServerHandler.Servers.Values.Where(x => x.IsRunning))
                        {
                            ServerHandler.StopServer(serv.Port, true);
                        }
                        Running = false;
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                    if (input == "status")
                    {
                        foreach (Server serv in ServerHandler.Servers.Values)
                        {
                            Print("Server@" + serv.Port + ":", ConsoleColor.Green);
                            Console.WriteLine(" " + (serv.IsRunning ? "Busy" : "Standby"));
                        }
                    }
                } Thread.Sleep(500);
            }
 
        }
        static void DataRecieved(Server server, byte Type, byte[] Data, IPEndPoint Sender)
        {
         //   Print("Server@" + server.Port + ": ",ConsoleColor.Green);
         //   Console.WriteLine("Recieved data (" + Type + ")");
            switch (Type)
            {
                #region Login
                case MessageType.LOGIN:
                    {
                        var message = Server.Deserialize<NetMessage>(Data);
                        bool result = false;
                        try
                        {
                            result = Database.ValidateUser((string)message.Data[0], (string) message.Data[1]);
                        }
                        catch { result =  false; }

                        var response = (new NetMessage() { Type = MessageType.LOGIN, Data = new object[]{result}});
                        Master.Send<NetMessage>(Sender, response, MessageType.LOGIN);
                    }
                    break;
                #endregion
                #region Match Making
                case MessageType.MACTHMAKING:
                    {
                        var request = Server.Deserialize<NetMessage>(Data);
                        switch ((byte)request.Data[0])
                        {
                            case MatchMaking._1VS1:
                                _1VS1Queue.Add(Sender);
                                while (_1VS1Queue.Count >= 2)
                                {
                                    IPEndPoint[] Recievers = _1VS1Queue.Take(2).ToArray();
                                    _1VS1Queue.RemoveRange(0, 2);
                                    int ID = ServerHandler.StartServer();
                                    //ServerHandler.Servers[ID].Players.AddRange(Recievers);
                                    Master.Send<NetMessage>(Recievers, new NetMessage() { Type = MessageType.MACTHMAKING, Data = new object[] { ID } }, MessageType.MACTHMAKING);
                                }
                                break;
                            case MatchMaking._2VS2:
                                _2VS2Queue.Add(Sender);
                                while (_2VS2Queue.Count >= 4)
                                {
                                    IPEndPoint[] Recievers = _2VS2Queue.Take(4).ToArray();
                                    _2VS2Queue.RemoveRange(0, 4);
                                    int ID = ServerHandler.StartServer();
                                   // ServerHandler.Servers[ID].Players.AddRange(Recievers);
                                    Master.Send<NetMessage>(Recievers, new NetMessage() { Type = MessageType.MACTHMAKING, Data = new object[] { ID } }, MessageType.MACTHMAKING);
                                    
                                }
                                break;
                            case MatchMaking._3VS3:
                                _3VS3Queue.Add(Sender);
                                while (_3VS3Queue.Count >= 6)
                                {
                                    IPEndPoint[] Recievers = _3VS3Queue.Take(6).ToArray();
                                    _3VS3Queue.RemoveRange(0, 6);
                                    int ID = ServerHandler.StartServer();
                                    //ServerHandler.Servers[ID].Players.AddRange(Recievers);
                                    Master.Send<NetMessage>(Recievers, new NetMessage() { Type = MessageType.MACTHMAKING, Data = new object[] { ID } }, MessageType.MACTHMAKING);
                                }
                                break;
                        }

                    }
                    break;
                #endregion
                #region CHARACTERSELECT
                case MessageType.CHARACTERSELECT :
                    server.Send<NetMessage>(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Server.Deserialize<NetMessage>(Data), MessageType.CHARACTERSELECT);
                    int SelectedCount = (int)server.Data[Server.GameData.CharacterSelected];
                    SelectedCount++;
                    server.Data[Server.GameData.CharacterSelected] = SelectedCount;
                    
                    if (SelectedCount == server.Players.Count)
                    {
                        for(int i = 0; i < server.Players.Count/2; i++)
                        {
                             server.Send<NetMessage>(server.Players[i], new NetMessage() {Data = new object[]{Team.Blue} }, MessageType.GAMESTART);
                        }
                        for (int i = server.Players.Count / 2; i < server.Players.Count; i++)
                        {
                            server.Send<NetMessage>(server.Players[i], new NetMessage() { Data = new object[] { Team.Red } }, MessageType.GAMESTART);
                        }
                       
                    }
                    break;
                #endregion 
                case MessageType.GAMESTART:
                    {
                        var player = Server.Deserialize<Player>(Data);
                        ((List<Player>)server.Data[Server.GameData.Players]).Add(player);
                    }
                    break;
                #region Message
                case MessageType.MESSAGE:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.MESSAGE);
                    break;
                case MessageType.HELLO:
                    server.Players.Add(Sender);
                    break;
                #endregion
                case MessageType.POSITION:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(),Data, MessageType.POSITION);
                    break;
                case MessageType.ABILITY_PROJECTILE:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.ABILITY_PROJECTILE);
                    break;
                case MessageType.PROJECTILE_DESTROYED:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.PROJECTILE_DESTROYED);
                    break;
                case MessageType.PROJECTILE_HIT:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.PROJECTILE_HIT);
                    break;
                case MessageType.ABILITY_FIELD :
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.ABILITY_FIELD );
                    break;
                case MessageType.FIELD_DESTROYED:
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.FIELD_DESTROYED);
                    break;
                case MessageType.FIELD_HIT :
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.FIELD_HIT);
                    break;
                case MessageType.APPLY_EFFECT :
                    server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.APPLY_EFFECT);
                    break;
                case MessageType.DEATH:
                    {
                        var deathplayer = Server.Deserialize<Player>(Data);
                        List<Player> players = (List<Player>)server.Data[Server.GameData.Players];
                        players.RemoveAll((x) => x.Equals(deathplayer));
                        int count = players.Count((x) => x.Team == deathplayer.Team && !x.Player_Death);
                        if (count <= 0) {
                            Team win = deathplayer.Team == Team.Blue ? Team.Red : Team.Blue;
                            List<Team> round = (List<Team>)server.Data[Server.GameData.Round];
                            round.Add(win);
                            if (round.Count((x) => x == win) >= 2)
                            {
                                server.Send<NetMessage>(server.Players.ToArray(), new NetMessage() { Data = new object[] { win } }, MessageType.GAMEDONE);
                                System.Timers.Timer timer = new System.Timers.Timer(3000);
                                timer.Elapsed += (sender, args) => { server.Stop(false); timer.Stop(); };
                                timer.Start();
                            }
                            else server.Send<NetMessage>(server.Players.ToArray(), new NetMessage() { Data = new object[] { deathplayer.Team } }, MessageType.ROUNDOVER);
                        }
                        server.Send(server.Players.Where(x => x.Equals(Sender) == false).ToArray(), Data, MessageType.DEATH);
                        break;
                    }
            }
        }
        public static void PrintLine(string Text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(Text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static void Print(string Text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(Text);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
   
}
