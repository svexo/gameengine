﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameEngine.Physics.Collision;
using System.Threading;

namespace GameEngine.Base
{
    public class Menu
    {
        private struct ButtonsInfo
        {
            public Button button;
            public bool MouseOnButton;
            public ButtonsInfo(Button button)
            {
                this.button = button;
                this.MouseOnButton = false;
            }
            public void SetMouseOnButton(bool value) { MouseOnButton = value; }
        }
        private List<Button> Buttons = new List<Button>();
        public bool Visible { get; private set; }
        private bool Events = true;
        public Menu()
        {
            Visible = false;
        }
        public void DisableEvents()
        {
            Events = false;
        }
        public void EnableEvents()
        {
            Events = true;
        }
        public Menu(Button[] buttons)
        {
            Visible = false;
            AddButton(buttons);
        }
        public void AddButton(Button Button)
        {
            Buttons.Add(Button);
        }
        public void AddButton(Button[] buttons)
        {
            foreach (Button butt in buttons)
            {
                Buttons.Add(butt);
            }
        }
        public void Show()
        {
            this.EnableEvents();
            this.Visible = true;
        }
        public void Hide()
        {
            this.Visible = false;
        }
        public void Clear()
        {  
            Buttons.Clear();
        }
        public virtual void Update()
        {
            if (!Visible || !Events) return;
            var mouseState = Mouse.GetState();
            for (int i = 0; i < Buttons.Count;i++)
            {
                Button but = Buttons[i];
                if ((mouseState.X > but.Position.X) && (mouseState.X < but.Position.X + but.Font.MeasureString(but.Text).X) &&
                        (mouseState.Y > but.Position.Y) && (mouseState.Y < but.Position.Y + but.Font.MeasureString(but.Text).Y))
                {
                    if (mouseState.LeftButton == ButtonState.Pressed)
                    {
                        if (but.onClick != null) but.onClick(but, new EventArgs());
                    }
                    else
                    {
                        if (!Buttons[i].MouseOnButton)
                        {
                            Buttons[i].MouseOnButton = true;
                            if (but.onMouseEnter != null) but.onMouseEnter(but, new EventArgs());
                        }
                    }
                }
                else
                {
                    if (Buttons[i].MouseOnButton)
                    {
                        Buttons[i].MouseOnButton = false;
                        if(but.onMouseLeave != null) but.onMouseLeave(but, new EventArgs());
                    }
                }
            }


        }
        public void Centralize(int ButtonDistance, int TotalHeight, int TotalWidth)
        {
            Vector2 ButtonSize = Vector2.Zero;
            if (Buttons.Count > 0)
            {
                ButtonSize = Buttons.First().Font.MeasureString(Buttons.First().Text);
            }
            float ButtonStartY = TotalHeight / 2 - ((ButtonSize.Y + ButtonDistance) * Buttons.Count) / 2;
            foreach (Button but in Buttons)
            {
                ButtonSize = but.Font.MeasureString(but.Text);
                float ButtonStartX = TotalWidth / 2 - (ButtonSize.X / 2);
                but.Position = new Vector2(ButtonStartX, ButtonStartY);
                ButtonStartY += ButtonDistance + ButtonSize.Y;
            }

        }
        public virtual void Draw(SpriteBatch Sprite)
        {
            if (!Visible) return;
            for (int i = 0; i < Buttons.Count; i++)
            {
                Sprite.DrawString(Buttons[i].Font, Buttons[i].Text, Buttons[i].Position, Buttons[i].TextColor);
            }
        }
    }
    public class Button
    {
        public string Text { get; set; }
        public Vector2 Position { get; set; }
        public Color TextColor { get; set; }
        public SpriteFont Font { get; set; }
        public EventHandler onMouseEnter;
        public EventHandler onMouseLeave;
        public EventHandler onClick;
        internal bool MouseOnButton { get; set; }
        public Button()
        {
            MouseOnButton = false;
        }
    }
    public class MenuHandler
    {
        private Dictionary<string, Menu> Menus = new Dictionary<string, Menu>();
        public Menu CurrentMenu { get; private set; }
        public MenuHandler()
        {
            CurrentMenu = null;
        }
        public bool MenuVisible
        {
            get
            {
                if (CurrentMenu == null) return false;
                else return CurrentMenu.Visible;
            }
        }
        public void Update()
        {
            if (CurrentMenu != null)
                 CurrentMenu.Update();
        }
        public void Draw(SpriteBatch Sprite)
        {
            if (CurrentMenu != null)
                CurrentMenu.Draw(Sprite);
        }
        public void RegisterMenu(string Name,Menu Menu)
        {
            if (Menus.ContainsKey(Name)) throw new Exception("There is already a menu registered with this name!");
            else Menus.Add(Name, Menu);
        }
        public void UpdateMenu(string Name, Menu Menu)
        {
            if (Menus.ContainsKey(Name))
                Menus[Name] = Menu;
            else throw new Exception(string.Format("There is no Menu with the name:\"{0}\" registered.", Name));
        }
        public void ShowMenu(string Name)
        {
            if (Menus.ContainsKey(Name))
            {
                CurrentMenu = Menus[Name];
                Thread.Sleep(300);
                CurrentMenu.Show();
            }
            else throw new Exception(string.Format("There is not Menu with the name:\"{0}\" registered.", Name));
        }
        public void HideMenu()
        {
            CurrentMenu = null;
        }
        public void Centrialize(int buttonDistance,int Height,int Width)
        {
            if (CurrentMenu == null) return;
            CurrentMenu.Centralize(buttonDistance,Height,Width);
        }

    }
}
