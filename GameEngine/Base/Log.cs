﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GameEngine.Base
{
    public class Log
    {
        private StreamWriter Writer;
        private string FilePath;
        public Log(string FilePath)
        {
            this.FilePath = FilePath;
        }
        public void Write(string txt)
        {
                using (Writer = new StreamWriter(FilePath, true))
                {
                    Writer.WriteLine(string.Format("{0}  >  {1}", DateTime.Now.ToString("G"), txt));
                }
            
        }
    }
}
