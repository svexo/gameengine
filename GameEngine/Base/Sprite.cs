﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace GameEngine.Base
{
    public class Sprite 
    {
        public Texture2D Texture { get; private set; }
        private double TimeInterval { get; set; }
        private double TimeRemaining { get; set; }
        private int FramesX { get; set; }
        private int FramesY { get; set; }
        public  int FrameHeight { get;private set; }
        public int FrameWidth { get;private set; }
        public int TotalFrames { get;private set; }
        public int CurrentFrame { get; private set; }
        public Vector2 Origin { get { return new Vector2(FrameWidth / 2, FrameHeight / 2); } }
        public Rectangle FrameBounds
        {
            get
            {
                return new Rectangle(CurrentFrame % FramesX * FrameWidth,
                    CurrentFrame / FramesX * FrameHeight,
                    FrameWidth,FrameHeight);
            }
        }
        public Sprite(Texture2D SpirteSheet, int FramesX, int FramesY, int FrameRate) :
            this(SpirteSheet, FramesX, FramesY, FrameRate, FramesX * FramesY) { }

        public Sprite(Texture2D SpirteSheet, int FramesX, int FramesY, double FrameRate, int TotalFrames)
        {
            this.FramesX = FramesX;
            this.FramesY = FramesY;
            this.TotalFrames = TotalFrames;
            this.TimeInterval = 1 / FrameRate;
            this.Texture = SpirteSheet;
            this.FrameWidth = SpirteSheet.Width / FramesX;
            this.FrameHeight = SpirteSheet.Height / FramesY;
            this.TimeRemaining = this.TimeInterval;
        }
        public virtual void Update(GameTime Time)
        {
            this.TimeRemaining -= Time.ElapsedGameTime.TotalSeconds;
            if (this.TimeRemaining <= 0)
            {
                this.CurrentFrame++;
                this.CurrentFrame %= TotalFrames;
                this.TimeRemaining = TimeInterval;
            }
        }
        public virtual void Draw(SpriteBatch Sprite,Vector2 Position)
        {
            Sprite.Draw(Texture, Position, FrameBounds, Color.White, 0f, Origin, 1f, SpriteEffects.None, 0f);
        }
    }
}
