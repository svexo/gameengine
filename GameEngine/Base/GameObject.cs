﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameEngine.Physics.Collision;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
namespace GameEngine.Base
{
    public class GameObject : ICollision
    {
        private static int objects = 0;
        private static CollisionHandler CollisionHandler = new CollisionHandler();
        protected static List<GameObject> Nodes = new List<GameObject>();
        public virtual float layerDepth { get { return 0.1f; } }
        private int ID { get; set; }
        public Vector2 Position { get; set; }
        public virtual Texture2D Texture { get; set; }
        public virtual Rectangle Bounds { get { return new Rectangle((int)this.Position.X - (int)this.Origin.X,
                                                (int)this.Position.Y - (int)this.Origin.Y,
                                                this.Width, this.Height); } }
        public virtual int Width { get { return Texture.Width; } }
        public virtual int Height { get { return Texture.Height; } }
        public Vector2 Origin { get { return new Vector2(Width / 2, Height / 2); } }
        public bool Visible { get; set; }
        public int AplhaCollision { get; set; }
        public float Rotation { get; set; }
        public GameObject(bool Collision)
        {
            ID = objects;
            objects++;
            Visible = true;
            AplhaCollision = 45;
            Rotation = 0f;
            Nodes.Insert(0, this);
            CollisionHandler.Add(this);
         
        }
        public GameObject() { }
        public virtual void OnCollide(GameObject X)
        {
            
        }
        public virtual void Draw(SpriteBatch spriteBatch,GameTime gameTime)
        {
            spriteBatch.Draw(Texture, Position, null, Color.White,Rotation, Origin,1f, SpriteEffects.None, layerDepth);
    
            //spriteBatch.Draw(Texture,Position,Bounds,
        }
        public virtual void Update(GameTime gameTime)
        {

        }
        public float GetDistance(GameObject obj)
        {
           return Physics.Physics.CalculateDistance(this.Origin, obj.Origin);
        }
        public static void UpdateAll(GameTime gameTime)
        {

            for (int i = 0; i < Nodes.Count; i++)
            {
                Nodes[i].Update(gameTime);
            }
            CollisionHandler.CheckCollisions();
        }
        public static void DrawAll(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].Visible)
                {
                    Nodes[i].Draw(spriteBatch, gameTime);
                }
            }
        }
        public virtual void Destroy()
        {
            CollisionHandler.Remove(this);
            Nodes.Remove(this);
        }
        public override bool Equals(object obj)
        {
            var Object = obj as GameObject;
            if (Object == null) return false;
            if (Object.ID != this.ID) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return this.ID;
        }
        public virtual void Move(Vector2 Direction)
        {
            this.Position += Direction;
        }

    }
    public class DynamicObject : GameObject
    {
        public DynamicObject() {Speed = 3; }

        public Vector2 Velocity { get; set; }
        public DynamicObject(bool Collision) : base(Collision) { Speed = 3; }
        public float Speed { get; set; }
        public override void Update(GameTime time)
        {
            Position += (Velocity * Speed);
        }
        public override void OnCollide(GameObject Object)
        {
            if (Object as DynamicObject == null)
            {
                this.Position -= this.Velocity;
            }
           
        }
    }
    public class SpriteObject : DynamicObject
    {
        public SpriteObject(bool Collision, Sprite Animation)
            : base(Collision)
        {
            this.Animation = Animation;
        }
        public override int Height
        {
            get
            {
                return Animation.FrameHeight;
            }
        }
        public override int Width
        {
            get
            {
                return Animation.FrameHeight;
            }
        }
        public override Texture2D Texture
        {
            get
            {
                return Animation.Texture;
            }
        }
        public Sprite Animation { get; set; }

        public override void Update(GameTime time)
        {
            base.Update(time);
            Animation.Update(time);
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            Animation.Draw(spriteBatch, Position);
        }
    }

}
