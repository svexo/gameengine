﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameEngine.Base
{
    public static class ActionHandler
    {
        private static List<EngineAction> Tasks = new List<EngineAction>();
        private static Timer Timer = new Timer(1000);
        public static void Start()
        {
            Timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            Timer.Start();
        }

        static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < Tasks.Count; i++)
            {
                if(Tasks[i].Time != EngineAction.InfiniteTime)
                   Tasks[i].Time--;
                if (Tasks[i].Time <= 0)
                {
                    if (Tasks[i].onActionDone != null) Tasks[i].onActionDone(Tasks[i], new EventArgs());
                    Tasks.RemoveAt(i);
                }
            }
        }
        public static void AddAction(EngineAction Action)
        {
            Tasks.Add(Action);
        }

        public static void RemoveAction(EngineAction Action)
        {
            Tasks.Remove(Action);
        }
        public static void Draw(SpriteBatch sprite,GameTime time)
        {
            for(int i = 0;i < Tasks.Count;i++)
            {
                if (Tasks[i].onDraw != null)
                    Tasks[i].onDraw(sprite, time);
            }
        }
        public static void Update(GameTime time)
        {
            for (int i = 0; i < Tasks.Count; i++)
            {
                if (Tasks[i].onUpdate != null)
                    Tasks[i].onUpdate(time);
            }
        }
    }
    public class EngineAction
    {
        public Action<SpriteBatch, GameTime> onDraw { get; set; }
        public Action<GameTime> onUpdate { get; set; }
        public EventHandler onActionDone;
        /// <summary>
        /// In Seconds!
        /// </summary>
        public int Time { get; set; }
        public const int InfiniteTime = int.MaxValue;
    }
}
