﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System.Timers;
using XnaMediaPlayer;
using Microsoft.Xna.Framework.Media;

namespace GameEngine.Base
{
    public class SoundPlayer
    {
        public static float Volume { get; set; }
        private static Timer timer;
        public SoundPlayer()
        {
            Volume = 0.75f;
        }
        public static void PlaySoundEffect(SoundEffect Effect)
        {
            Effect.CreateInstance().Play();
        }
        public static void PlaySoundEffect(SoundEffect SEffect, int Time)
        {
            SoundEffectInstance Effect = SEffect.CreateInstance();
            Effect.IsLooped = true;
            Effect.Play();
            timer = new Timer((double)Time);
            timer.Start(); timer.Elapsed += delegate(object x, ElapsedEventArgs e)
            {
                if(Effect.State == SoundState.Playing)
                   Effect.Stop();
            };
        }
        public static void PlaySong(Song Song,bool loop = false)
        {
            MediaPlayer.Play(Song);
            MediaPlayer.IsRepeating = loop;
        }      
        private SongCollection Collection;
        public void SetPlayList(SongCollection Songs)
        {
            Collection = Songs;
            MediaPlayer.Play(Songs);
        }
        public void Next()
        {
            MediaPlayer.MoveNext();
        }
        public static void Stop()
        {
            MediaPlayer.IsRepeating = false;
            MediaPlayer.Stop();
        }
        public void PlaySongFromPlayList(int Index)
        {
            if(Index >= Collection.Count)throw new IndexOutOfRangeException();
            MediaPlayer.Play(Collection, Index);
        }   
    }
}
