﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Timers;
using Microsoft.Xna.Framework;


namespace GameEngine.Base
{
    public static class Utilities
    {
        public static void DrawMessage(string Text, SpriteFont Font,Vector2 Position,Color color,int Time)
        {
            EngineAction action = new EngineAction();
            action.onDraw = (SpriteBatch Sprite,GameTime GameTime) =>
            {
                    Sprite.DrawString(Font, Text,Position, color);
            };
            action.Time = Time;
            ActionHandler.AddAction(action);
        }
        public static void DrawSprite(Sprite sprite, Vector2 Position, int Repeat = 1)
        {
            int index = 0; bool framelock = true;
            EngineAction action = new EngineAction();
            action.onDraw = (SpriteBatch Sprite, GameTime GameTime) =>
            {
                sprite.Draw(Sprite, Position);
            };
            action.onUpdate = (GameTime gameTime) =>
            {
                sprite.Update(gameTime);
                if (sprite.CurrentFrame == sprite.TotalFrames - 1 )
                {

                    if (framelock)
                    {

                        index++;
                        if (index >= Repeat)
                        {
                            ActionHandler.RemoveAction(action);
                        }
                        framelock = false;
                    }
                    
                    
                }
                if (sprite.CurrentFrame == 0) framelock = true;
            };
            action.Time = EngineAction.InfiniteTime;
            ActionHandler.AddAction(action);
        }

    }
}
