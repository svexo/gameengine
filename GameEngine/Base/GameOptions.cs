﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Extension;
using GameEngine.Networking;
namespace GameEngine.Base
{
    public static class GameOptions
    {
        public static int GameWidth { get; set; }
        public static int GameHeight { get; set; }
        public static Dictionary<object, Keys> Controls = new Dictionary<object, Keys>();
        public static void SetDefaults(int GameWidth , int GameHeight,ref GraphicsDeviceManager game)
        {
            GameOptions.GameHeight = GameHeight;
            GameOptions.GameWidth = GameWidth;
            game.PreferredBackBufferWidth = GameWidth;
            game.PreferredBackBufferHeight = GameHeight;
        }
        public static void LoadOptions(string File)
        {
            try
            {
                lock (Controls)
                {
                    using (BinaryReader Reader = new BinaryReader(new FileStream(File, FileMode.Create)))
                    {
                        int count = Reader.ReadInt32();
                        for (int i = 0; i < count; i++)
                        {
                            int objcount = Reader.ReadInt32();
                            byte[] objdata = Reader.ReadBytes(objcount);
                            int keydata = Reader.ReadInt32();
                            Controls.Add(objdata.FromArray(), (Keys)Enum.Parse(typeof(Keys), keydata.ToString()));

                        }
                    }
                }
            }
            catch (FileNotFoundException x)
            {
                throw x; // LOAD DEFAULT + WARNING
            }
            catch (Exception x)
            {
                throw new Exception("Save File Corrupted", x);
            }
        }
        public static void SaveOptions(string File)
        {
            lock (Controls)
            {
                using (BinaryWriter Writer = new BinaryWriter(new FileStream(File, FileMode.Open)))
                {
                    Writer.Write(Controls.Count);
                    foreach (var Entry in Controls)
                    {
                        byte[] obj = Entry.Key.ToArray();
                        Writer.Write(obj.Length);
                        Writer.Write(obj, 0, obj.Length);
                        Writer.Write((int)Entry.Value);
                    }
                }
            }


        }
    }
}
namespace Extension
{
    internal static class ObjectSerialization
    {
        internal static byte[] ToArray(this object x)
        {
            using(var Stream =new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(Stream, x);
                return Stream.ToArray();
            }
        }
        internal static object FromArray(this byte[] y)
        {
            using (var Stream = new MemoryStream(y))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return formatter.Deserialize(Stream);
            }
        }
    }
}
