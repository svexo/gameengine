﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine.Base;
namespace GameEngine.Physics.Collision
{
     internal class CollisionHandler
    {
        private List<GameObject> Objects = new List<GameObject>();
        public void Add(GameObject Object)
        {
            Objects.Add(Object);
        }
        public void CheckCollisions()
        {
            for(int i = 0; i < Objects.Count;i++)
            {
                CheckCollision(Objects[i]);
            }
            //Parallel.ForEach<GameObject>(Objects, new ParallelOptions() { MaxDegreeOfParallelism = 10 }, x => CheckCollision(x));  ==> NOT ALWATS IN SYNC WITH GAME THREAD !!!!!!!!
        }
        public void Remove(GameObject Object)
        {
            Objects.Remove(Object);
        }
        private void CheckCollision(GameObject Object)
        {

            for (int i = 0; i < Objects.Count; i++)
            {
                if (!Object.Equals(Objects[i]))
                {
                    if (Collision.Collide(Object, Objects[i]))
                    {
                        try
                        {
                            Object.OnCollide(Objects[i]);
                            Objects[i].OnCollide(Object);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            //DO NOTHING
                        }
                    }
                }
            }
        }
    }
}
