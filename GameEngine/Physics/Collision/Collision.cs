﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameEngine.Base;

namespace GameEngine.Physics.Collision
{
    public static class Collision
    {
        public static Rectangle Intersect(Rectangle ObjA,Rectangle ObjB)
        {
            int x1 = Math.Max(ObjA.Left, ObjB.Left); 
            int y1 = Math.Max(ObjA.Top, ObjB.Top);
            int x2 = Math.Min(ObjA.Right, ObjB.Right); 
            int y2 = Math.Min(ObjA.Bottom, ObjB.Bottom);
            int width = x2 - x1;
            int height = y2 - y1; 
            if (width > 0 && height > 0) 
                return new Rectangle(x1, y1, width, height);
            else return Rectangle.Empty;
        }
        public static Rectangle NormilizeBounds(ICollision Obj,Rectangle Rect )
        {
            return new Rectangle(Rect.X - (int)Obj.Position.X + (int)Obj.Origin.X, Rect.Y - (int)Obj.Position.Y + (int)Obj.Origin.Y, Rect.Width, Rect.Height);
        }
        public static bool Collide(ICollision ObjectA, ICollision ObjectB)
        {
            Rectangle CollisionRect = Intersect(ObjectA.Bounds, ObjectB.Bounds);
            if (CollisionRect == Rectangle.Empty) return false;
            int PixelCount = CollisionRect.Width * CollisionRect.Height;
            Color[] PixelsA = new Color[PixelCount];
            Color[] PixelsB = new Color[PixelCount];
            ObjectA.Texture.GetData<Color>(0, NormilizeBounds(ObjectA,CollisionRect), PixelsA, 0, PixelsA.Length);
            ObjectB.Texture.GetData<Color>(0, NormilizeBounds(ObjectB,CollisionRect), PixelsB, 0, PixelsB.Length);
           
            for (int i = 0; i < PixelCount; i++)
            {
                if (PixelsA[i].A > ObjectA.AplhaCollision && PixelsB[i].A > ObjectB.AplhaCollision) return true;
            }
            return false;

        }


    }
    public interface ICollision
    {
        void OnCollide(GameObject Object);
        Vector2 Position { get;}
        Texture2D Texture{get;}
        Rectangle Bounds { get;}
        Vector2 Origin { get; }
        int Width {get;}
        int Height {get;}
        int AplhaCollision { get; }
    }
}
