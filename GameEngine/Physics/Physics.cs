﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameEngine.Physics
{
    public static class Physics
    {
        
        public static float CalculateRotation(Vector2 PointA, Vector2 PointB)
        {

                double A = Math.Pow((PointB.X - PointA.X), 2);
                double B = Math.Pow((PointB.Y - PointA.Y), 2);
                double CSquared = A + B;
                double c = (float)Math.Sqrt(CSquared);
                if (PointA.Y > PointB.Y)
                { 
                    return (float)Math.Asin(((PointB.X - PointA.X)) / c);
                }
                else
                { 

                    if (PointA.X > PointB.X)
                    {
                        return -(float)Math.Acos((PointA.Y - PointB.Y) / c);
                    }
                    else
                    {
                        return (float)Math.Acos((PointA.Y - PointB.Y) / c);
                    }
                }
  
        }
        public static Vector2 CalculateDirection(Vector2 PointA, Vector2 PointB)
        {
            float angle = Physics.CalculateRotation(PointA, PointB);
            return new Vector2((float)Math.Sin(angle), -(float)Math.Cos(angle));
        }
        public static float CalculateDistance(Vector2 PointA, Vector2 PointB)
        {
            double A = Math.Pow((PointB.X - PointA.X), 2);
            double B = Math.Pow((PointB.Y - PointA.Y), 2);
            double C = A + B;
            
            return (float)Math.Sqrt(C);
        }
    }
}
