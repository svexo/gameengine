﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace GameEngine.Networking
{
    public abstract class Serialization
    {
        private static BinaryFormatter Formatter = new BinaryFormatter();
        public static byte[] Serialize<T>(T Obj) where T : ISerializable
        {
            byte[] data;
            using (var Mem = new MemoryStream())
            {
                Formatter.Serialize(Mem, Obj);
                data = Mem.ToArray();
            }
            return data;
        }
        public static T Deserialize<T>(byte[] Data) where T : ISerializable
        {
            using (var ms = new MemoryStream(Data))
            {
                return (T)Formatter.Deserialize(ms);
            }
        }
    }
}
