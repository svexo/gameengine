﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameEngine.Networking
{
    public class OnRecieve : EventArgs
    {
        public byte[] Data { get; private set; }
        public byte Type { get; private set; }
        public OnRecieve(byte[] Data, byte Type)
        {
            this.Data = Data;
            this.Type = Type;
        }
    }
}
