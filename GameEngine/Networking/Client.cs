﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization;
namespace GameEngine.Networking
{
    public class Client : Serialization
    {
        public EventHandler<OnRecieve> DataRecieved;
        private UdpClient ClientSide;
        public bool Running { get; private set; }
        private Thread ListenThread;

        public int Port { get; private set; }
        public Client(string ServerIP, int Port)
        {
            this.Port = Port;
            ClientSide = new UdpClient(ServerIP, Port);
            ClientSide.Connect(ServerIP, Port);
        }
        public void Start()
        {
            Running = true;
  
            ListenThread = new Thread(new ThreadStart(Listen));
            ListenThread.Start();
        }
        public void Send<T>(byte Type, T Obj) where T : ISerializable
        {
            var Data = Client.Serialize<T>(Obj);
            ClientSide.Send(AddRequestType(Type, Data), Data.Length + 1);
        }
        private byte[] AddRequestType(byte Type, byte[] Data)
        {
            byte[] data = new byte[Data.Length + 1];
            data[0] = Type;
            for (int i = 1; i < data.Length; i++)
            {
                data[i] = Data[i - 1];
            }
            return data;
        }
        private void GetRequestType(byte[] OrginalData, out byte[] ObjectData, out byte Type)
        {
            ObjectData = new byte[OrginalData.Length - 1];
            for (int i = 1; i < OrginalData.Length; i++)
            {
                ObjectData[i - 1] = OrginalData[i];
            }
            Type = OrginalData[0];
        }
        private void Listen()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, Port);
            while (Running)
            {
                try
                {
                    var Data = ClientSide.Receive(ref sender);
                    byte type;
                    byte[] data;
                    GetRequestType(Data, out data, out type);
                    DataRecieved(sender, new OnRecieve(data, type));
                }
                catch { }
            }
        }
        public void Stop(bool ForceKill = false)
        {
            Running = false;
            if (ForceKill) ListenThread.Abort();

        }
    }
}
