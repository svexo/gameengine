﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GameEngine.Networking
{
    [Serializable()]
    public class NetMessage : ISerializable
    {
        public byte Type { get; set; }
        public object[] Data { get; set; }
        public NetMessage()
        {

        }
        public NetMessage(SerializationInfo info, StreamingContext context)
        {
            Type = (byte)info.GetValue("Type", (typeof(byte)));
            Data = (object[])info.GetValue("Data", typeof(object[]));

        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Type", Type);
            info.AddValue("Data", Data);
        }
    }
}
